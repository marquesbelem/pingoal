﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IAPModal : MonoBehaviour
{

    public void Buy6000Gold()
    {
        IAPManager.Instance.Buy6000Gold();
    }

    public void Buy12000Gold()
    {
        IAPManager.Instance.Buy12000Gold();
    }
    public void Buy18000Gold()
    {
        IAPManager.Instance.Buy18000Gold();
    }
    public void Buy36000Gold()
    {
        IAPManager.Instance.Buy36000Gold();
    }
    public void Buy72000Gold()
    {
        IAPManager.Instance.Buy72000Gold();
    }

}
