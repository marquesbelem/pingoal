﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColliderPontos : MonoBehaviour
{
    public string nomePonto;
    private Game game;
    void Start()
    {
        game = FindObjectOfType<Game>();
    }

    void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.gameObject.tag == "Bola" && nomePonto == "jogadorDeBaixo")
        {
            Debug.Log("colediu");
            if (coll.gameObject.GetComponent<Ball>().bolaDefault)
            {
                game.pPlayer += 1;
                Game.ladoEmY = -1;
                Game.ladoEmX = 1;
                //Game.setBall = true;
                game.Pingooal();

            }
            else
            {
                game.pPlayer += 1;
                coll.gameObject.GetComponent<Ball>().BolaDuplaForaCena();
                BallContagem.contagem = 0;
            }
        }
        if (coll.gameObject.tag == "Bola" && nomePonto == "jogadorCima")
        {
            Debug.Log("colediu");
            if (coll.gameObject.GetComponent<Ball>().bolaDefault)
            {
                game.pBot += 1;
                Game.ladoEmY = 1;
                Game.ladoEmX = 0;
                //Game.setBall = true;
                game.Pingooal();
            }
            else
            {
                game.pBot += 1;
                coll.gameObject.GetComponent<Ball>().BolaDuplaForaCena();
                BallContagem.contagem = 0;
            }
        }
    }

}