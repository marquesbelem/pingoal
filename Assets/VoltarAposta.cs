﻿using UnityEngine;
using UnityEngine.UI;
public class VoltarAposta : MonoBehaviour {

    public Text valorApostaText;
    void Start () {
        valorApostaText.text = "Aposta: " + PlayerPrefs.GetInt("ValorAposta").ToString();
    }

    public void VoltarApostas()
    {
        int aposta = PlayerPrefs.GetInt("ValorAposta");
        int moedasP = PlayerPrefs.GetInt("ApostaTotal");
        moedasP += aposta;
        PlayerPrefs.SetInt("ApostaTotal", moedasP);
        PlayerPrefs.SetInt("ValorAposta", 0);

    }
}
