﻿#if UNITY_ANDROID
using UnityEngine;
using UnityEngine.Advertisements;
public class ADSUnity : MonoBehaviour
{

    public GameObject btnRecompensa;

    [SerializeField]
    private string id;

    public void OnShow(string zonaID)
    {
        if (string.IsNullOrEmpty(zonaID)) //dizer se os videos são pulaveis 
            zonaID = id;

        ShowOptions option = new ShowOptions(); //verifica o status do anuncio
        option.resultCallback = HandleShowResult;

        Advertisement.Show(zonaID, option);
    }

    void HandleShowResult(ShowResult result)
    {
        switch (result)
        {
            case ShowResult.Failed:
                break;
            case ShowResult.Skipped:
                break;
            case ShowResult.Finished:
                int total = PlayerPrefs.GetInt("ApostaTotal");
                total += 120;
                PlayerPrefs.SetInt("ApostaTotal", total);
                break;

        }
    }

    private int idMoedas;
    void RecompensaMoedas()
    {
        idMoedas = Random.Range(1, 3);

        switch (idMoedas)
        {
            case 1:
                int total = PlayerPrefs.GetInt("ApostaTotal");
                total += 100;
                PlayerPrefs.SetInt("ApostaTotal", total);
                break;
            case 2:
                int total2 = PlayerPrefs.GetInt("ApostaTotal");
                total2 += 150;
                PlayerPrefs.SetInt("ApostaTotal", total2);
                break;
        }
    }
}
 #endif