﻿using UnityEngine;
using UnityEngine.UI;
public class Skill : MonoBehaviour {

    public static int speedGoal;
    public static int pontos = 5;
    public int maxP;
    public int minP;
    public Text pText;

    public Button[] bMais;
    public Button[] bMenos;

	void Update()
    {
        SetText();
        Buttons();

    }

    void Start()
    {
        goleiroPontos.rectTransform.sizeDelta = new Vector2(valueGoleiro, 41);
        bolaCurvaPontos.rectTransform.sizeDelta = new Vector2(velueCurvaBall, 41);
        bolaSpeedPontos.rectTransform.sizeDelta = new Vector2(velueSpeedBall, 41);
    }
    public float valorSpeedGoleiro;
    public Image goleiroPontos;
    public static float valueGoleiro;
    //Função principal para setar a velocidade no goleiro
	void SpeedGoalkeeper(float v)
    {
        if(v > 0)
            Goalkeeper.speed += valorSpeedGoleiro;
        else
            Goalkeeper.speed -= valorSpeedGoleiro;
    }
    //Função do botão mais
    public void SpeedGMais(int value)
    {
       
        goleiroPontos.rectTransform.sizeDelta += new Vector2(21.2f, 0); 
        SpeedGoalkeeper(value);
        AtValue(value,"goal");
        AtualizaPontosTotal(value);
        valueGoleiro = goleiroPontos.rectTransform.sizeDelta.x;
    }
    //Função do botão menos
    public void SpeedGMenos(int value)
    {
        goleiroPontos.rectTransform.sizeDelta -= new Vector2(21, 0); 
        SpeedGoalkeeper(value);
        AtValue(value,"goal");
        AtualizaPontosTotal(value);
        valueGoleiro = goleiroPontos.rectTransform.sizeDelta.x;
    }

    void SetText()
    {
        pText.text = pontos.ToString("000");
    }

    public static int speedBall;
    public Image bolaSpeedPontos;
    public float valorDoSpeedBall;
    public static float velueSpeedBall;
    void SpeedBall(int v)
    {
        if (v > 0)
            Ball.massB -= valorDoSpeedBall;
        else
            Ball.massB += valorDoSpeedBall;
    }

    public void SpeedBMais(int value)
    {
        bolaSpeedPontos.rectTransform.sizeDelta += new Vector2( 21.2f, 0);
        SpeedBall(value);
        AtValue(value, "ball");
        AtualizaPontosTotal(value);
        velueSpeedBall = bolaSpeedPontos.rectTransform.sizeDelta.x;
    }

    public void SpeedBMenos(int value)
    {
        bolaSpeedPontos.rectTransform.sizeDelta -= new Vector2( 21f, 0);
        SpeedBall(value);
        AtValue(value, "ball");
        AtualizaPontosTotal(value);
        velueSpeedBall = bolaSpeedPontos.rectTransform.sizeDelta.x;
    }

    public static int curvaBall;
    public float valueCurvaBola;
    public Image bolaCurvaPontos;
    public static float velueCurvaBall;
    void CurvaBall(int v)
    {
        if (v > 0)
            Ball.rotaValue += valueCurvaBola;
        else
            Ball.rotaValue -= valueCurvaBola;
    }

    public void CurvaBMais(int value)
    {
        bolaCurvaPontos.rectTransform.sizeDelta += new Vector2(21.2f, 0);
        CurvaBall(value);
        AtValue(value, "curva");
        AtualizaPontosTotal(value);
        velueCurvaBall = bolaCurvaPontos.rectTransform.sizeDelta.x;
    }

    public void CurvaBMenos(int value)
    {
        bolaCurvaPontos.rectTransform.sizeDelta -= new Vector2(21f, 0);
        CurvaBall(value);
        AtValue(value, "curva");
        AtualizaPontosTotal(value);
        velueCurvaBall = bolaCurvaPontos.rectTransform.sizeDelta.x;
    }

    void AtualizaPontosTotal(int v)
    {
        if (v > 0)
            pontos -= 1;
        else
            pontos += 1;

        if (pontos < minP)
            pontos = minP;
        if (pontos > maxP)
            pontos = maxP;
        
    }

    //função para atualizar os valores dos pontos de cada função 
    void AtValue(int v, string tipo)
    {
        if(v < 0)
        {
            switch (tipo)
            {
                case "goal":
                        speedGoal -= 1;
                    break;
                case "ball":
                        speedBall -= 1;
                    break;
                case "curva":
                    curvaBall -= 1;
                    break;
            }
        }
        else
        {
            switch (tipo)
            {
                case "goal":
                        speedGoal += 1;
                    break;
                case "ball":
                        speedBall += 1;
                    break;
                case "curva":
                    curvaBall += 1;
                    break;
            }
        }
        

        if (speedGoal < minP)
            speedGoal = minP;
   
        if (speedBall < minP)
            speedBall = minP;

        if (curvaBall < minP)
            curvaBall = minP;

    }

    void Buttons()
    {

        if (pontos > minP && pontos <= maxP)
        {
            foreach (Button b in bMais)
            {
                b.interactable = true;
            }
        }
        else
        {
            foreach (Button b in bMais)
            {
                b.interactable = false;
            }
        }

        if (speedGoal == minP)
            bMenos[0].interactable = false;
        else
            bMenos[0].interactable = true;

        if (speedBall == minP)
            bMenos[1].interactable = false;
        else
            bMenos[1].interactable = true;

        if (curvaBall == minP)
            bMenos[2].interactable = false;
        else
            bMenos[2].interactable = true;
    }
}
