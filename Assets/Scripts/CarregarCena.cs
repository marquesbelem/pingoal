﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class CarregarCena : MonoBehaviour {

    public void Carregar(string scene)
    {
        SceneManager.LoadScene(scene);
    }
}
