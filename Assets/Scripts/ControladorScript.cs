﻿using System.Collections;
using UnityEngine;

public class ControladorScript : MonoBehaviour
{
    public NavCtrl1 nav;
    void Start()
    {
        nav = FindObjectOfType<NavCtrl1>();
        StartCoroutine("StartPlay");
    }

    IEnumerator StartPlay()
    {
   
       
        yield return new WaitForSeconds(0.9f);
        nav.enabled = true;
        yield return new WaitForSeconds(1f);
        enabled = false;
    }
}