﻿using UnityEngine;

public class InstaciarFormacaoOnline : MonoBehaviour
{

    [Header("Time Player")]
    public GameObject posPL;
    public GameObject palhetaL;
    private string pLeftInstanciar;

    public GameObject posPR;
    public GameObject palhetaR;
    private string pRigthtInstanciar;

    public GameObject posGoleiro;
    public GameObject goleiro;
    private string goleiroInstanciar;

    [Header("Arena")]
    public GameObject posFundo;
    private GameObject fundo;
    private string fundoInstanciar;

    public GameObject posMascara;
    private GameObject mascara;
    private string mascaraInstanciar;
    public string hostOuConvidado;

    public GameObject gl;
    public GameObject gr;
    public GameObject gf;
    public GameObject gm;
    public GameObject gg;
    
    void Start()
    {
        InstanciarObjetos();
        this.enabled = false;
    }


    public void InstanciarObjetos()
    {

        gl = Instantiate(palhetaL, posPL.transform.position, Quaternion.identity);
        gr = Instantiate(palhetaR, posPR.transform.position, Quaternion.identity);
        // gf = Instantiate(fundo, posFundo.transform.position, Quaternion.identity);
        //gm = Instantiate(mascara, posMascara.transform.position, Quaternion.identity);
        gg = Instantiate(goleiro, posGoleiro.transform.position, Quaternion.identity);

        gl.transform.parent = posPL.transform;
        gr.transform.parent = posPR.transform;
        //gf.transform.parent = posFundo.transform;
        //gm.transform.parent = posMascara.transform;
        gg.transform.parent = posGoleiro.transform;

        //JointMotor2D jml = gl.GetComponent<HingeJoint2D>().motor;
        //jml.motorSpeed = -1000f;
        //gl.GetComponent<HingeJoint2D>().motor = jml;

        //JointMotor2D jmr = gr.GetComponent<HingeJoint2D>().motor;
        //jmr.motorSpeed = 1000f;
        //gr.GetComponent<HingeJoint2D>().motor = jmr;
        
        gl.GetComponent<PalhetaL>().enabled = false;
        gr.GetComponent<PalhetaR>().enabled = false;
    
       }

    }

