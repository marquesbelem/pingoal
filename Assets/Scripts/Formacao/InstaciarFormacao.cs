﻿using UnityEngine;

public class InstaciarFormacao : MonoBehaviour
{

    [Header("Time Player")]
    public GameObject posPL;
    private GameObject palhetaL;
    private string pLeftInstanciar;

    public GameObject posPR;
    private GameObject palhetaR;
    private string pRigthtInstanciar;

    public GameObject posGoleiro;
    private GameObject goleiro;
    private string goleiroInstanciar;

    [Header("Arena")]
    public GameObject posFundo;
    private GameObject fundo;
    private string fundoInstanciar;

    public GameObject posMascara;
    private GameObject mascara;
    private string mascaraInstanciar;
    public bool menu;
    public bool multiplayerOnline;
    public string hostOuConvidado;

    private GameObject gl;
    private GameObject gr;
    private GameObject gf;
    private GameObject gm;
    private GameObject gg;

    void Start()
    {
        LoadPrefabs();
    }

    public void LoadPrefabs()
    {
        if (!menu)
        {
            pLeftInstanciar = PlayerPrefs.GetString("pLeftInstanciar");

            if (pLeftInstanciar == "" || pLeftInstanciar == "PalhetaPadrao")
                palhetaL = Resources.Load<GameObject>("PPadrao/Left");

            if (pLeftInstanciar == "PalhetaAmarela")
                palhetaL = Resources.Load<GameObject>("PPadrao/PalhetaAmarela_Left");

            if (pLeftInstanciar == "PalhetaRosa")
                palhetaL = Resources.Load<GameObject>("PPadrao/PalhetaRosa_Left");

            if (pLeftInstanciar == "PalhetaRoxa")
                palhetaL = Resources.Load<GameObject>("PPadrao/PalhetaRoxa_Left");

            if (pLeftInstanciar == "PalhetaVerde")
                palhetaL = Resources.Load<GameObject>("PPadrao/PalhetaVerde_Left");

            if (pLeftInstanciar == "PalhetaVermelha")
                palhetaL = Resources.Load<GameObject>("PPadrao/Left1");

            if (pLeftInstanciar == "PalhetaFantasma")
                palhetaL = Resources.Load<GameObject>("PPadrao/Fantasma_Left");
            
            if (pLeftInstanciar == "PalhetaBolinhasAmarelo")
                palhetaL = Resources.Load<GameObject>("PPadrao/PalhetaBolinhasAmarelo_Left");

            if (pLeftInstanciar == "PalhetaBolinhasAzul")
                palhetaL = Resources.Load<GameObject>("PPadrao/PalhetaBolinhasAzul_Left");

            if (pLeftInstanciar == "PalhetaBolinhasVerde")
                palhetaL = Resources.Load<GameObject>("PPadrao/PalhetaBolinhasVerde_Left");

            if (pLeftInstanciar == "PalhetaBolinhasVermelho")
                palhetaL = Resources.Load<GameObject>("PPadrao/PalhetaBolinhasVermelho_Left");

            if (pLeftInstanciar == "PalhetaFantasmaAzul")
                palhetaL = Resources.Load<GameObject>("PPadrao/PalhetaFantasmaAzul_Left");

            if (pLeftInstanciar == "PalhetaFantasmaVerde")
                palhetaL = Resources.Load<GameObject>("PPadrao/PalhetaFantasmaVerde_Left");

            if (pLeftInstanciar == "PalhetaFantasmaVermelho")
                palhetaL = Resources.Load<GameObject>("PPadrao/PalhetaFantasmaVermelho_Left");

            if (pLeftInstanciar == "PalhetaGelo")
                palhetaL = Resources.Load<GameObject>("PPadrao/PalhetaGelo_Left");

            if (pLeftInstanciar == "PalhetaPsiquicaVermelha")
                palhetaL = Resources.Load<GameObject>("PPadrao/PalhetaPsiquicaVermelha_Left");

            if (pLeftInstanciar == "PalhetaPsiquicaVerde")
                palhetaL = Resources.Load<GameObject>("PPadrao/PalhetaPsiquicaVerde_Left");

            if (pLeftInstanciar == "PalhetaPsiquica")
                palhetaL = Resources.Load<GameObject>("PPadrao/PalhetaPsiquica_Left");

            if (pLeftInstanciar == "PalhetaPsiquicaAzul")
                palhetaL = Resources.Load<GameObject>("PPadrao/PalhetaPsiquicaAzul_Left");

            if (pLeftInstanciar == "PalhetaGeloAmarela")
                palhetaL = Resources.Load<GameObject>("PPadrao/PalhetaGeloAmarela_Left");

            if (pLeftInstanciar == "PalhetaGeloRoxa")
                palhetaL = Resources.Load<GameObject>("PPadrao/PalhetaGeloRoxa_Left");

            if (pLeftInstanciar == "PalhetaGeloVerde")
                palhetaL = Resources.Load<GameObject>("PPadrao/PalhetaGeloVerde_Left");

            pRigthtInstanciar = PlayerPrefs.GetString("pRigthtInstanciar");

            if (pRigthtInstanciar == "" || pRigthtInstanciar == "PalhetaPadrao")
                palhetaR = Resources.Load<GameObject>("PPadrao/Rigth");

            if (pRigthtInstanciar == "PalhetaAmarela")
                palhetaR = Resources.Load<GameObject>("PPadrao/PalhetaAmarela_Rigth");

            if (pRigthtInstanciar == "PalhetaRosa")
                palhetaR = Resources.Load<GameObject>("PPadrao/PalhetaRosa_Rigth");

            if (pRigthtInstanciar == "PalhetaRoxa")
                palhetaR = Resources.Load<GameObject>("PPadrao/PalhetaRoxa_Rigth");

            if (pRigthtInstanciar == "PalhetaVerde")
                palhetaR = Resources.Load<GameObject>("PPadrao/PalhetaVerde_Rigth");

            if (pRigthtInstanciar == "PalhetaVermelha")
                palhetaR = Resources.Load<GameObject>("PPadrao/Rigth1");

            if (pRigthtInstanciar == "PalhetaFantasma")
                palhetaR = Resources.Load<GameObject>("PPadrao/Fantasma_Rigth");
            
            if (pRigthtInstanciar == "PalhetaBolinhasAmarelo")
                palhetaR = Resources.Load<GameObject>("PPadrao/PalhetaBolinhasAmarelo_Rigth");

            if (pRigthtInstanciar == "PalhetaBolinhasAzul")
                palhetaR = Resources.Load<GameObject>("PPadrao/PalhetaBolinhasAzul_Rigth");

            if (pRigthtInstanciar == "PalhetaBolinhasVerde")
                palhetaR = Resources.Load<GameObject>("PPadrao/PalhetaBolinhasVerde_Rigth");

            if (pRigthtInstanciar == "PalhetaBolinhasVermelho")
                palhetaR = Resources.Load<GameObject>("PPadrao/PalhetaBolinhasVermelho_Rigth");

            if (pRigthtInstanciar == "PalhetaFantasmaAzul")
                palhetaR = Resources.Load<GameObject>("PPadrao/PalhetaFantasmaAzul_Rigth");

            if (pRigthtInstanciar == "PalhetaFantasmaVerde")
                palhetaR = Resources.Load<GameObject>("PPadrao/PalhetaFantasmaVerde_Rigth");

            if (pRigthtInstanciar == "PalhetaFantasmaVermelho")
                palhetaR = Resources.Load<GameObject>("PPadrao/PalhetaFantasmaVermelho_Rigth");

            if (pRigthtInstanciar == "PalhetaGelo")
                palhetaR = Resources.Load<GameObject>("PPadrao/PalhetaGelo_Rigth");

            if (pRigthtInstanciar == "PalhetaPsiquicaAzul")
                palhetaR = Resources.Load<GameObject>("PPadrao/PalhetaPsiquicaAzul_Rigth");

            if (pRigthtInstanciar == "PalhetaPsiquica")
                palhetaR = Resources.Load<GameObject>("PPadrao/PalhetaPsiquica_Rigth");

            if (pRigthtInstanciar == "PalhetaPsiquicaVerde")
                palhetaR = Resources.Load<GameObject>("PPadrao/PalhetaPsiquicaVerde_Rigth");

            if (pRigthtInstanciar == "PalhetaPsiquicaVermelha")
                palhetaR = Resources.Load<GameObject>("PPadrao/PalhetaPsiquicaVermelha_Rigth");

            if (pRigthtInstanciar == "PalhetaGeloVerde")
                palhetaR = Resources.Load<GameObject>("PPadrao/PalhetaGeloVerde_Rigth");

            if (pRigthtInstanciar == "PalhetaGeloRoxa")
                palhetaR = Resources.Load<GameObject>("PPadrao/PalhetaGeloRoxa_Rigth");

            if (pRigthtInstanciar == "PalhetaGeloAmarela")
                palhetaR = Resources.Load<GameObject>("PPadrao/PalhetaGeloAmarela_Rigth");


            goleiroInstanciar = PlayerPrefs.GetString("goleiroInstanciar");

            if (goleiroInstanciar == "" || goleiroInstanciar == "GoleiroPadrao")
                goleiro = Resources.Load<GameObject>("PGoleiro/GoleiroPadrao");

            if (goleiroInstanciar == "GoleiroAtaquePreto")
                goleiro = Resources.Load<GameObject>("PGoleiro/GoleiroAtaquePreto");

            if (goleiroInstanciar == "GoleiroRightPreto")
                goleiro = Resources.Load<GameObject>("PGoleiro/GoleiroRightPreto");

            if (goleiroInstanciar == "GoleiroBasicoVerde")
                goleiro = Resources.Load<GameObject>("PGoleiro/GoleiroBasicoVerde");

            if (goleiroInstanciar == "GoleiroLeftPreto")
                goleiro = Resources.Load<GameObject>("PGoleiro/GoleiroLeftPreto");

            if (goleiroInstanciar == "GoleiroBasicoPreto")
                goleiro = Resources.Load<GameObject>("PGoleiro/GoleiroBasicoPreto");

            if (goleiroInstanciar == "GoleiroBasicoAmarelo")
                goleiro = Resources.Load<GameObject>("PGoleiro/GoleiroBasicoAmarelo");

            if (goleiroInstanciar == "GoleiroPadraoVermelho")
                goleiro = Resources.Load<GameObject>("PGoleiro/GoleiroPadraoVermelho");
        }
        else
        {
            pLeftInstanciar = PlayerPrefs.GetString("pLeftInstanciar");

            if (pLeftInstanciar == "" || pLeftInstanciar == "PalhetaPadrao")
                palhetaL = Resources.Load<GameObject>("PPadraoBot/Left");
            if (pLeftInstanciar == "PalhetaAmarela")
                palhetaL = Resources.Load<GameObject>("PPadraoBot/PalhetaAmarela_Left");
            if (pLeftInstanciar == "PalhetaRosa")
                palhetaL = Resources.Load<GameObject>("PPadraoBot/PalhetaRosa_Left");
            if (pLeftInstanciar == "PalhetaRoxa")
                palhetaL = Resources.Load<GameObject>("PPadraoBot/PalhetaRoxa_Left");
            if (pLeftInstanciar == "PalhetaVerde")
                palhetaL = Resources.Load<GameObject>("PPadraoBot/PalhetaVerde_Left");
            if (pLeftInstanciar == "PalhetaVermelha")
                palhetaL = Resources.Load<GameObject>("PPadraoBot/Left1");
            if (pLeftInstanciar == "PalhetaFantasma")
                palhetaL = Resources.Load<GameObject>("PPadraoBot/Fantasma_Left");
            if (pLeftInstanciar == "PalhetaBolinhasAmarelo")
                palhetaL = Resources.Load<GameObject>("PPadraoBot/PalhetaBolinhasAmarelo_Left");
            if (pLeftInstanciar == "PalhetaBolinhasAzul")
                palhetaL = Resources.Load<GameObject>("PPadraoBot/PalhetaBolinhasAzul_Left");
            if (pLeftInstanciar == "PalhetaBolinhasVerde")
                palhetaL = Resources.Load<GameObject>("PPadraoBot/PalhetaBolinhasVerde_Left");
            if (pLeftInstanciar == "PalhetaBolinhasVermelho")
                palhetaL = Resources.Load<GameObject>("PPadraoBot/PalhetaBolinhasVermelho_Left");
            if (pLeftInstanciar == "PalhetaFantasmaAzul")
                palhetaL = Resources.Load<GameObject>("PPadraoBot/PalhetaFantasmaAzul_Left");
            if (pLeftInstanciar == "PalhetaFantasmaVerde")
                palhetaL = Resources.Load<GameObject>("PPadraoBot/PalhetaFantasmaVerde_Left");
            if (pLeftInstanciar == "PalhetaFantasmaVermelho")
                palhetaL = Resources.Load<GameObject>("PPadraoBot/PalhetaFantasmaVermelho_Left");
            if (pLeftInstanciar == "PalhetaGelo")
                palhetaL = Resources.Load<GameObject>("PPadraoBot/PalhetaGelo_Left");
            if (pLeftInstanciar == "PalhetaPsiquicaVermelha")
                palhetaL = Resources.Load<GameObject>("PPadraoBot/PalhetaPsiquicaVermelha_Left");
            if (pLeftInstanciar == "PalhetaPsiquicaVerde")
                palhetaL = Resources.Load<GameObject>("PPadraoBot/PalhetaPsiquicaVerde_Left");
            if (pLeftInstanciar == "PalhetaPsiquica")
                palhetaL = Resources.Load<GameObject>("PPadraoBot/PalhetaPsiquica_Left");
            if (pLeftInstanciar == "PalhetaPsiquicaAzul")
                palhetaL = Resources.Load<GameObject>("PPadraoBot/PalhetaPsiquicaAzul_Left");
            if (pLeftInstanciar == "PalhetaGeloAmarela")
                palhetaL = Resources.Load<GameObject>("PPadraoBot/PalhetaGeloAmarela_Left");
            if (pLeftInstanciar == "PalhetaGeloRoxa")
                palhetaL = Resources.Load<GameObject>("PPadraoBot/PalhetaGeloRoxa_Left");
            if (pLeftInstanciar == "PalhetaGeloVerde")
                palhetaL = Resources.Load<GameObject>("PPadraoBot/PalhetaGeloVerde_Left");
            pRigthtInstanciar = PlayerPrefs.GetString("pRigthtInstanciar");

            if (pRigthtInstanciar == "" || pRigthtInstanciar == "PalhetaPadrao")
                palhetaR = Resources.Load<GameObject>("PPadraoBot/Rigth");
            if (pRigthtInstanciar == "PalhetaAmarela")
                palhetaR = Resources.Load<GameObject>("PPadraoBot/PalhetaAmarela_Rigth");
            if (pRigthtInstanciar == "PalhetaRosa")
                palhetaR = Resources.Load<GameObject>("PPadraoBot/PalhetaRosa_Rigth");
            if (pRigthtInstanciar == "PalhetaRoxa")
                palhetaR = Resources.Load<GameObject>("PPadraoBot/PalhetaRoxa_Rigth");
            if (pRigthtInstanciar == "PalhetaVerde")
                palhetaR = Resources.Load<GameObject>("PPadraoBot/PalhetaVerde_Rigth");
            if (pRigthtInstanciar == "PalhetaVermelha")
                palhetaR = Resources.Load<GameObject>("PPadraoBot/Rigth1");
            if (pRigthtInstanciar == "PalhetaFantasma")
                palhetaR = Resources.Load<GameObject>("PPadraoBot/Fantasma_Rigth");
            if (pRigthtInstanciar == "PalhetaBolinhasAmarelo")
                palhetaR = Resources.Load<GameObject>("PPadraoBot/PalhetaBolinhasAmarelo_Rigth");
            if (pRigthtInstanciar == "PalhetaBolinhasAzul")
                palhetaR = Resources.Load<GameObject>("PPadraoBot/PalhetaBolinhasAzul_Rigth");
            if (pRigthtInstanciar == "PalhetaBolinhasVerde")
                palhetaR = Resources.Load<GameObject>("PPadraoBot/PalhetaBolinhasVerde_Rigth");
            if (pRigthtInstanciar == "PalhetaBolinhasVermelho")
                palhetaR = Resources.Load<GameObject>("PPadraoBot/PalhetaBolinhasVermelho_Rigth");
            if (pRigthtInstanciar == "PalhetaFantasmaAzul")
                palhetaR = Resources.Load<GameObject>("PPadraoBot/PalhetaFantasmaAzul_Rigth");
            if (pRigthtInstanciar == "PalhetaFantasmaVerde")
                palhetaR = Resources.Load<GameObject>("PPadraoBot/PalhetaFantasmaVerde_Rigth");
            if (pRigthtInstanciar == "PalhetaFantasmaVermelho")
                palhetaR = Resources.Load<GameObject>("PPadraoBot/PalhetaFantasmaVermelho_Rigth");
            if (pRigthtInstanciar == "PalhetaGelo")
                palhetaR = Resources.Load<GameObject>("PPadraoBot/PalhetaGelo_Rigth");
            if (pRigthtInstanciar == "PalhetaPsiquicaAzul")
                palhetaR = Resources.Load<GameObject>("PPadraoBot/PalhetaPsiquicaAzul_Rigth");
            if (pRigthtInstanciar == "PalhetaPsiquica")
                palhetaR = Resources.Load<GameObject>("PPadraoBot/PalhetaPsiquica_Rigth");
            if (pRigthtInstanciar == "PalhetaPsiquicaVerde")
                palhetaR = Resources.Load<GameObject>("PPadraoBot/PalhetaPsiquicaVerde_Rigth");
            if (pRigthtInstanciar == "PalhetaPsiquicaVermelha")
                palhetaR = Resources.Load<GameObject>("PPadraoBot/PalhetaPsiquicaVermelha_Rigth");
            if (pRigthtInstanciar == "PalhetaGeloVerde")
                palhetaR = Resources.Load<GameObject>("PPadraoBot/PalhetaGeloVerde_Rigth");
            if (pRigthtInstanciar == "PalhetaGeloRoxa")
                palhetaR = Resources.Load<GameObject>("PPadraoBot/PalhetaGeloRoxa_Rigth");
            if (pRigthtInstanciar == "PalhetaGeloAmarela")
                palhetaR = Resources.Load<GameObject>("PPadraoBot/PalhetaGeloAmarela_Rigth");
            goleiroInstanciar = PlayerPrefs.GetString("goleiroInstanciar");

            if (goleiroInstanciar == "" || goleiroInstanciar == "GoleiroPadrao")
                goleiro = Resources.Load<GameObject>("PGoleiroBot/GoleiroPadrao");
            if (goleiroInstanciar == "GoleiroAtaquePreto")
                goleiro = Resources.Load<GameObject>("PGoleiroBot/GoleiroAtaquePreto");
            if (goleiroInstanciar == "GoleiroRightPreto")
                goleiro = Resources.Load<GameObject>("PGoleiroBot/GoleiroRightPreto");
            if (goleiroInstanciar == "GoleiroBasicoVerde")
                goleiro = Resources.Load<GameObject>("PGoleiroBot/GoleiroBasicoVerde");
            if (goleiroInstanciar == "GoleiroLeftPreto")
                goleiro = Resources.Load<GameObject>("PGoleiroBot/GoleiroLeftPreto");
            if (goleiroInstanciar == "GoleiroBasicoPreto")
                goleiro = Resources.Load<GameObject>("PGoleiroBot/GoleiroBasicoPreto");
            if (goleiroInstanciar == "GoleiroBasicoAmarelo")
                goleiro = Resources.Load<GameObject>("PGoleiroBot/GoleiroBasicoAmarelo");
            if (goleiroInstanciar == "GoleiroPadraoVermelho")
                goleiro = Resources.Load<GameObject>("PGoleiroBot/GoleiroPadraoVermelho");
        }


        fundoInstanciar = PlayerPrefs.GetString("fundoInstanciar");

        if (fundoInstanciar == "" || fundoInstanciar == "FundoPadrao")
            fundo = Resources.Load<GameObject>("PArena/FundoPadrao");
        if (fundoInstanciar == "FundoGelo")
            fundo = Resources.Load<GameObject>("PArena/FundoGelo");
        if (fundoInstanciar == "FundoMadeira")
            fundo = Resources.Load<GameObject>("PArena/FundoMadeira");
        if (fundoInstanciar == "FundoMetal")
            fundo = Resources.Load<GameObject>("PArena/FundoMetal");
        if (fundoInstanciar == "FundoAirHockey")
            fundo = Resources.Load<GameObject>("PArena/FundoAirHockey");
        if (fundoInstanciar == "FundoAzulAmarelo")
            fundo = Resources.Load<GameObject>("PArena/FundoAzulAmarelo");
        if (fundoInstanciar == "FundoBlueCircles")
            fundo = Resources.Load<GameObject>("PArena/FundoBlueCircles");
        if (fundoInstanciar == "FundoFutebol")
            fundo = Resources.Load<GameObject>("PArena/FundoFutebol");
        if (fundoInstanciar == "FundoFutsal")
            fundo = Resources.Load<GameObject>("PArena/FundoFutsal");
        if (fundoInstanciar == "FundoPedra")
            fundo = Resources.Load<GameObject>("PArena/FundoPedra");
        if (fundoInstanciar == "FundoRosa")
            fundo = Resources.Load<GameObject>("PArena/FundoRosa");
        if (fundoInstanciar == "FundoTijolo")
            fundo = Resources.Load<GameObject>("PArena/FundoTijolo");

        mascaraInstanciar = PlayerPrefs.GetString("mascaraInstanciar");

        if (mascaraInstanciar == "" || mascaraInstanciar == "MascaraPadrao")
            mascara = Resources.Load<GameObject>("PArena/MascaraPadrao");
        if (mascaraInstanciar == "MascaraGelo")
            mascara = Resources.Load<GameObject>("PArena/MascaraGelo");
        if (mascaraInstanciar == "MascaraMadeira")
            mascara = Resources.Load<GameObject>("PArena/MascaraMadeira");
        if (mascaraInstanciar == "MascaraMetal")
            mascara = Resources.Load<GameObject>("PArena/MascaraMetal");
        if (mascaraInstanciar == "MascaraAirHockey")
            mascara = Resources.Load<GameObject>("PArena/MascaraAirHockey");
        if (mascaraInstanciar == "MascaraAzulAmarelo")
            mascara = Resources.Load<GameObject>("PArena/MascaraAzulAmarelo");
        if (mascaraInstanciar == "MascaraBlueCircles")
            mascara = Resources.Load<GameObject>("PArena/MascaraBlueCircles");
        if (mascaraInstanciar == "MascaraFutebol")
            mascara = Resources.Load<GameObject>("PArena/MascaraFutebol");
        if (mascaraInstanciar == "MascaraFutsal")
            mascara = Resources.Load<GameObject>("PArena/MascaraFutsal");
        if (mascaraInstanciar == "MascaraPedra")
            mascara = Resources.Load<GameObject>("PArena/MascaraPedra");
        if (mascaraInstanciar == "MascaraRosa")
            mascara = Resources.Load<GameObject>("PArena/MascaraRosa");
        if (mascaraInstanciar == "MascaraTijolo")
            mascara = Resources.Load<GameObject>("PArena/MascaraTijolo");


        InstanciarObjetos();

    }
   
    void InstanciarObjetos()
    {

       /* if (multiplayerOnline)
        {
            SendNomes(palhetaL.name, palhetaR.name,goleiro.name);
        }*/

        gl = Instantiate(palhetaL, posPL.transform.position, Quaternion.identity);
        gr = Instantiate(palhetaR, posPR.transform.position, Quaternion.identity);
        gf = Instantiate(fundo, posFundo.transform.position, Quaternion.identity);
        gm = Instantiate(mascara, posMascara.transform.position, Quaternion.identity);
        gg = Instantiate(goleiro, posGoleiro.transform.position, Quaternion.identity);

        gl.transform.parent = posPL.transform;
        gr.transform.parent = posPR.transform;
        gf.transform.parent = posFundo.transform;
        gm.transform.parent = posMascara.transform;
        gg.transform.parent = posGoleiro.transform;
        gl.transform.rotation = palhetaL.transform.rotation;
        gr.transform.rotation = palhetaR.transform.rotation;

    }

}
