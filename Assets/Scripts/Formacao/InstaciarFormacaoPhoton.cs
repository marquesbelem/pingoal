﻿using UnityEngine;
public class InstaciarFormacaoPhoton : MonoBehaviour
{
    [Header("Time Player")]
    public GameObject palhetaL;
    private string pLeftInstanciar;
    
    public GameObject palhetaR;
    private string pRigthtInstanciar;
    
    public GameObject goleiro;
    private string goleiroInstanciar;

    [Header("Arena")]
    public GameObject posFundo;
    private GameObject fundo;
    private string fundoInstanciar;

    public GameObject posMascara;
    private GameObject mascara;
    private string mascaraInstanciar;

    private GameObject gl;
    private GameObject gr;
    private GameObject gf;
    private GameObject gm;
    private GameObject gg;
    
  
    private GameObject[] p;
    private GameObject[] g;
 
    void Start()
    {
        LoadPrefabs();
        
    }

    public void LoadPrefabs()
    {
        
            pLeftInstanciar = PlayerPrefs.GetString("pLeftInstanciar");

            if (pLeftInstanciar == "" || pLeftInstanciar == "PalhetaPadrao")
                palhetaL = Resources.Load<GameObject>("Left");

            if (pLeftInstanciar == "PalhetaAmarela")
                palhetaL = Resources.Load<GameObject>("PPadraoOnline/PalhetaAmarela_Left");

            if (pLeftInstanciar == "PalhetaRosa")
                palhetaL = Resources.Load<GameObject>("PPadraoOnline/PalhetaRosa_Left");

            if (pLeftInstanciar == "PalhetaRoxa")
                palhetaL = Resources.Load<GameObject>("PPadraoOnline/PalhetaRoxa_Left");

            if (pLeftInstanciar == "PalhetaVerde")
                palhetaL = Resources.Load<GameObject>("PPadraoOnline/PalhetaVerde_Left");

            if (pLeftInstanciar == "PalhetaVermelha")
                palhetaL = Resources.Load<GameObject>("Left1");

            if (pLeftInstanciar == "PalhetaFantasma")
                palhetaL = Resources.Load<GameObject>("PPadraoOnline/Fantasma_Left");

            if (pLeftInstanciar == "PalhetaBolinhasAmarelo")
                palhetaL = Resources.Load<GameObject>("PPadraoOnline/PalhetaBolinhasAmarelo_Left");

            if (pLeftInstanciar == "PalhetaBolinhasAzul")
                palhetaL = Resources.Load<GameObject>("PPadraoOnline/PalhetaBolinhasAzul_Left");

            if (pLeftInstanciar == "PalhetaBolinhasVerde")
                palhetaL = Resources.Load<GameObject>("PPadraoOnline/PalhetaBolinhasVerde_Left");

            if (pLeftInstanciar == "PalhetaBolinhasVermelho")
                palhetaL = Resources.Load<GameObject>("PPadraoOnline/PalhetaBolinhasVermelho_Left");

            if (pLeftInstanciar == "PalhetaFantasmaAzul")
                palhetaL = Resources.Load<GameObject>("PPadraoOnline/PalhetaFantasmaAzul_Left");

            if (pLeftInstanciar == "PalhetaFantasmaVerde")
                palhetaL = Resources.Load<GameObject>("PPadraoOnline/PalhetaFantasmaVerde_Left");

            if (pLeftInstanciar == "PalhetaFantasmaVermelho")
                palhetaL = Resources.Load<GameObject>("PPadraoOnline/PalhetaFantasmaVermelho_Left");

            if (pLeftInstanciar == "PalhetaGelo")
                palhetaL = Resources.Load<GameObject>("PPadraoOnline/PalhetaGelo_Left");

            if (pLeftInstanciar == "PalhetaPsiquicaVermelha")
                palhetaL = Resources.Load<GameObject>("PPadraoOnline/PalhetaPsiquicaVermelha_Left");

            if (pLeftInstanciar == "PalhetaPsiquicaVerde")
                palhetaL = Resources.Load<GameObject>("PPadraoOnline/PalhetaPsiquicaVerde_Left");

            if (pLeftInstanciar == "PalhetaPsiquica")
                palhetaL = Resources.Load<GameObject>("PPadraoOnline/PalhetaPsiquica_Left");

            if (pLeftInstanciar == "PalhetaPsiquicaAzul")
                palhetaL = Resources.Load<GameObject>("PPadraoOnline/PalhetaPsiquicaAzul_Left");

            if (pLeftInstanciar == "PalhetaGeloAmarela")
                palhetaL = Resources.Load<GameObject>("PPadraoOnline/PalhetaGeloAmarela_Left");

            if (pLeftInstanciar == "PalhetaGeloRoxa")
                palhetaL = Resources.Load<GameObject>("PPadraoOnline/PalhetaGeloRoxa_Left");

            if (pLeftInstanciar == "PalhetaGeloVerde")
                palhetaL = Resources.Load<GameObject>("PPadraoOnline/PalhetaGeloVerde_Left");

            pRigthtInstanciar = PlayerPrefs.GetString("pRigthtInstanciar");

            if (pRigthtInstanciar == "" || pRigthtInstanciar == "PalhetaPadrao")
                palhetaR = Resources.Load<GameObject>("Rigth");

            if (pRigthtInstanciar == "PalhetaAmarela")
                palhetaR = Resources.Load<GameObject>("PPadraoOnline/PalhetaAmarela_Rigth");

            if (pRigthtInstanciar == "PalhetaRosa")
                palhetaR = Resources.Load<GameObject>("PPadraoOnline/PalhetaRosa_Rigth");

            if (pRigthtInstanciar == "PalhetaRoxa")
                palhetaR = Resources.Load<GameObject>("PPadraoOnline/PalhetaRoxa_Rigth");

            if (pRigthtInstanciar == "PalhetaVerde")
                palhetaR = Resources.Load<GameObject>("PPadraoOnline/PalhetaVerde_Rigth");

            if (pRigthtInstanciar == "PalhetaVermelha")
                palhetaR = Resources.Load<GameObject>("Rigth1");

            if (pRigthtInstanciar == "PalhetaFantasma")
                palhetaR = Resources.Load<GameObject>("PPadraoOnline/Fantasma_Rigth");

            if (pRigthtInstanciar == "PalhetaBolinhasAmarelo")
                palhetaR = Resources.Load<GameObject>("PPadraoOnline/PalhetaBolinhasAmarelo_Rigth");

            if (pRigthtInstanciar == "PalhetaBolinhasAzul")
                palhetaR = Resources.Load<GameObject>("PPadraoOnline/PalhetaBolinhasAzul_Rigth");

            if (pRigthtInstanciar == "PalhetaBolinhasVerde")
                palhetaR = Resources.Load<GameObject>("PPadraoOnline/PalhetaBolinhasVerde_Rigth");

            if (pRigthtInstanciar == "PalhetaBolinhasVermelho")
                palhetaR = Resources.Load<GameObject>("PPadraoOnline/PalhetaBolinhasVermelho_Rigth");

            if (pRigthtInstanciar == "PalhetaFantasmaAzul")
                palhetaR = Resources.Load<GameObject>("PPadraoOnline/PalhetaFantasmaAzul_Rigth");

            if (pRigthtInstanciar == "PalhetaFantasmaVerde")
                palhetaR = Resources.Load<GameObject>("PPadraoOnline/PalhetaFantasmaVerde_Rigth");

            if (pRigthtInstanciar == "PalhetaFantasmaVermelho")
                palhetaR = Resources.Load<GameObject>("PPadraoOnline/PalhetaFantasmaVermelho_Rigth");

            if (pRigthtInstanciar == "PalhetaGelo")
                palhetaR = Resources.Load<GameObject>("PPadraoOnline/PalhetaGelo_Rigth");

            if (pRigthtInstanciar == "PalhetaPsiquicaAzul")
                palhetaR = Resources.Load<GameObject>("PPadraoOnline/PalhetaPsiquicaAzul_Rigth");

            if (pRigthtInstanciar == "PalhetaPsiquica")
                palhetaR = Resources.Load<GameObject>("PPadraoOnline/PalhetaPsiquica_Rigth");

            if (pRigthtInstanciar == "PalhetaPsiquicaVerde")
                palhetaR = Resources.Load<GameObject>("PPadraoOnline/PalhetaPsiquicaVerde_Rigth");

            if (pRigthtInstanciar == "PalhetaPsiquicaVermelha")
                palhetaR = Resources.Load<GameObject>("PPadraoOnline/PalhetaPsiquicaVermelha_Rigth");

            if (pRigthtInstanciar == "PalhetaGeloVerde")
                palhetaR = Resources.Load<GameObject>("PPadraoOnline/PalhetaGeloVerde_Rigth");

            if (pRigthtInstanciar == "PalhetaGeloRoxa")
                palhetaR = Resources.Load<GameObject>("PPadraoOnline/PalhetaGeloRoxa_Rigth");

            if (pRigthtInstanciar == "PalhetaGeloAmarela")
                palhetaR = Resources.Load<GameObject>("PPadraoOnlineOnline/PalhetaGeloAmarela_Rigth");


            goleiroInstanciar = PlayerPrefs.GetString("goleiroInstanciar");

            if (goleiroInstanciar == "" || goleiroInstanciar == "GoleiroPadrao")
                goleiro = Resources.Load<GameObject>("GoleiroPadrao");

            if (goleiroInstanciar == "GoleiroAtaquePreto")
                goleiro = Resources.Load<GameObject>("GoleiroAtaquePreto");

            if (goleiroInstanciar == "GoleiroRightPreto")
                goleiro = Resources.Load<GameObject>("GoleiroRightPreto");

            if (goleiroInstanciar == "GoleiroBasicoVerde")
                goleiro = Resources.Load<GameObject>("PGoleiroOnline/GoleiroBasicoVerde");

            if (goleiroInstanciar == "GoleiroLeftPreto")
                goleiro = Resources.Load<GameObject>("PGoleiroOnline/GoleiroLeftPreto");

            if (goleiroInstanciar == "GoleiroBasicoPreto")
                goleiro = Resources.Load<GameObject>("PGoleiroOnline/GoleiroBasicoPreto");

            if (goleiroInstanciar == "GoleiroBasicoAmarelo")
                goleiro = Resources.Load<GameObject>("PGoleiroOnline/GoleiroBasicoAmarelo");

            if (goleiroInstanciar == "GoleiroPadraoVermelho")
                goleiro = Resources.Load<GameObject>("PGoleiroOnline/GoleiroPadraoVermelho");

        fundoInstanciar = PlayerPrefs.GetString("fundoInstanciar");

        if (fundoInstanciar == "" || fundoInstanciar == "FundoPadrao")
            fundo = Resources.Load<GameObject>("PArena/FundoPadrao");
        if (fundoInstanciar == "FundoGelo")
            fundo = Resources.Load<GameObject>("PArena/FundoGelo");
        if (fundoInstanciar == "FundoMadeira")
            fundo = Resources.Load<GameObject>("PArena/FundoMadeira");
        if (fundoInstanciar == "FundoMetal")
            fundo = Resources.Load<GameObject>("PArena/FundoMetal");
        if (fundoInstanciar == "FundoAirHockey")
            fundo = Resources.Load<GameObject>("PArena/FundoAirHockey");
        if (fundoInstanciar == "FundoAzulAmarelo")
            fundo = Resources.Load<GameObject>("PArena/FundoAzulAmarelo");
        if (fundoInstanciar == "FundoBlueCircles")
            fundo = Resources.Load<GameObject>("PArena/FundoBlueCircles");
        if (fundoInstanciar == "FundoFutebol")
            fundo = Resources.Load<GameObject>("PArena/FundoFutebol");
        if (fundoInstanciar == "FundoFutsal")
            fundo = Resources.Load<GameObject>("PArena/FundoFutsal");
        if (fundoInstanciar == "FundoPedra")
            fundo = Resources.Load<GameObject>("PArena/FundoPedra");
        if (fundoInstanciar == "FundoRosa")
            fundo = Resources.Load<GameObject>("PArena/FundoRosa");
        if (fundoInstanciar == "FundoTijolo")
            fundo = Resources.Load<GameObject>("PArena/FundoTijolo");

        mascaraInstanciar = PlayerPrefs.GetString("mascaraInstanciar");

        if (mascaraInstanciar == "" || mascaraInstanciar == "MascaraPadrao")
            mascara = Resources.Load<GameObject>("PArena/MascaraPadrao");
        if (mascaraInstanciar == "MascaraGelo")
            mascara = Resources.Load<GameObject>("PArena/MascaraGelo");
        if (mascaraInstanciar == "MascaraMadeira")
            mascara = Resources.Load<GameObject>("PArena/MascaraMadeira");
        if (mascaraInstanciar == "MascaraMetal")
            mascara = Resources.Load<GameObject>("PArena/MascaraMetal");
        if (mascaraInstanciar == "MascaraAirHockey")
            mascara = Resources.Load<GameObject>("PArena/MascaraAirHockey");
        if (mascaraInstanciar == "MascaraAzulAmarelo")
            mascara = Resources.Load<GameObject>("PArena/MascaraAzulAmarelo");
        if (mascaraInstanciar == "MascaraBlueCircles")
            mascara = Resources.Load<GameObject>("PArena/MascaraBlueCircles");
        if (mascaraInstanciar == "MascaraFutebol")
            mascara = Resources.Load<GameObject>("PArena/MascaraFutebol");
        if (mascaraInstanciar == "MascaraFutsal")
            mascara = Resources.Load<GameObject>("PArena/MascaraFutsal");
        if (mascaraInstanciar == "MascaraPedra")
            mascara = Resources.Load<GameObject>("PArena/MascaraPedra");
        if (mascaraInstanciar == "MascaraRosa")
            mascara = Resources.Load<GameObject>("PArena/MascaraRosa");
        if (mascaraInstanciar == "MascaraTijolo")
            mascara = Resources.Load<GameObject>("PArena/MascaraTijolo");

        //Arena();
    }
   
    void Arena()
    {

        posFundo = GameObject.Find("Fundo");
        posMascara = GameObject.Find("Mascara");
        gf = Instantiate(fundo, posFundo.transform.position, Quaternion.identity);
        gm = Instantiate(mascara, posMascara.transform.position, Quaternion.identity);

        gf.transform.parent = posFundo.transform;
        gm.transform.parent = posMascara.transform;

        
    }
    
}


