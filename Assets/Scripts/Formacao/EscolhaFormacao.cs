﻿using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;
public class EscolhaFormacao : MonoBehaviour
{

    [Header("Time Player")]
    public Sprite[] obPLeft;
    public SpriteRenderer pLeft;
    private int seleAtualPL;
    public Button bPLNext;
    public Button bPLPrevius;

    public Sprite[] obPRigth;
    public SpriteRenderer pRigth;
    private int seleAtualPR;
    public Button bPRNext;
    public Button bPRPrevius;

    public Sprite[] obGoleiro;
    public SpriteRenderer pGoleiro;
    private int seleAtualGoleiro;
    public Button bGoNext;
    public Button bGoPrevius;

    [Header("Arena")]
    public Sprite[] obFundo;
    public SpriteRenderer sFundo;
    private int seleAtualFundo;
    public Button bFundoNext;
    public Button bFundoPrevius;

    public Sprite[] obMascara;
    public SpriteRenderer sMascara;
    private int seleAtualMascara;

    void Start()
    {
        seleAtualPL = PlayerPrefs.GetInt("seleAtualPL");
        seleAtualPR = PlayerPrefs.GetInt("seleAtualPR");
        seleAtualFundo = PlayerPrefs.GetInt("seleAtualFundo");
        seleAtualMascara = PlayerPrefs.GetInt("seleAtualMascara");
        seleAtualGoleiro = PlayerPrefs.GetInt("seleAtualGoleiro");

        AddSprite();

        obPLeft[0] = Resources.Load<Sprite>("PalhetaPadrao");
        obPRigth[0] = Resources.Load<Sprite>("PalhetaPadrao");
        obFundo[0] = Resources.Load<Sprite>("FundoPadrao");
        obMascara[0] = Resources.Load<Sprite>("MascaraPadrao");
        obGoleiro[0] = Resources.Load<Sprite>("GoleiroPadrao");

        obPLeft[1] = Resources.Load<Sprite>("PalhetaVermelha");
        obPRigth[1] = Resources.Load<Sprite>("PalhetaVermelha");
        obGoleiro[1] = Resources.Load<Sprite>("GoleiroPadraoVermelho");
    }

    void Update()
    {
        UpPLeft();
        UpPRigth();
        UpFundo();
        UpMascara();
        UpGoleiro();
    }

    void UpPLeft()
    {
        if (seleAtualPL == 0)
        {
            pLeft.sprite = obPLeft[seleAtualPL];
            bPLPrevius.interactable = false;
        }
        else
            bPLPrevius.interactable = true;

        if (seleAtualPL > 0 && seleAtualPL < obPLeft.Length - 1)
        {
            pLeft.sprite = obPLeft[seleAtualPL];
            bPLPrevius.interactable = true;
            bPLNext.interactable = true;
        }
        if (seleAtualPL >= obPLeft.Length - 1)
        {
            pLeft.sprite = obPLeft[seleAtualPL];
            bPLNext.interactable = false;
        }
        else
            bPLNext.interactable = true;


    }

    public void NextPLeft()
    {
        seleAtualPL = seleAtualPL + 1;

        if (obPLeft[seleAtualPL] == null)
            seleAtualPL = seleAtualPL + 1;

    }

    public void PreviusPLeft()
    {
        seleAtualPL = seleAtualPL - 1;

        if (obPLeft[seleAtualPL] == null)
            seleAtualPL = seleAtualPL - 1;
    }

    void UpPRigth()
    {
        if (seleAtualPR == 0)
        {
            pRigth.sprite = obPRigth[seleAtualPR];
            bPRPrevius.interactable = false;
        }
        else
            bPRPrevius.interactable = true;

        if (seleAtualPR > 0 && seleAtualPR < obPRigth.Length - 1)
        {
            pRigth.sprite = obPRigth[seleAtualPR];
            bPRPrevius.interactable = true;
            bPRNext.interactable = true;
        }
        if (seleAtualPR >= obPRigth.Length - 1)
        {
            pRigth.sprite = obPRigth[seleAtualPR];
            bPRNext.interactable = false;
        }
        else
            bPRNext.interactable = true;

    }

    public void NextRigth()
    {
        seleAtualPR = seleAtualPR + 1;

        if (obPRigth[seleAtualPR] == null)
        {
            seleAtualPR = seleAtualPR + 1;
        }
    }

    public void PreviusRigth()
    {
        seleAtualPR = seleAtualPR - 1;
        if (obPRigth[seleAtualPR] == null)
            seleAtualPR = seleAtualPL - 1;
    }

    void UpGoleiro()
    {
        if (seleAtualGoleiro == 0)
        {
            pGoleiro.sprite = obGoleiro[seleAtualGoleiro];
            bGoPrevius.interactable = false;
        }
        else
            bGoPrevius.interactable = true;

        if (seleAtualGoleiro > 0 && seleAtualGoleiro < obGoleiro.Length - 1)
        {
            pGoleiro.sprite = obGoleiro[seleAtualGoleiro];
            bGoPrevius.interactable = true;
            bGoNext.interactable = true;
        }
        if (seleAtualGoleiro >= obGoleiro.Length - 1)
        {
            pGoleiro.sprite = obGoleiro[seleAtualGoleiro];
            bGoNext.interactable = false;
        }
        else
            bGoNext.interactable = true;
    }

    public void NextGoleiro()
    {
        seleAtualGoleiro = seleAtualGoleiro + 1;
        if (obGoleiro[seleAtualGoleiro] == null)
            seleAtualGoleiro = seleAtualGoleiro + 1;
    }

    public void PreviusGoleiro()
    {
        seleAtualGoleiro = seleAtualGoleiro - 1;

        if (obGoleiro[seleAtualGoleiro] == null)
            seleAtualGoleiro = seleAtualGoleiro - 1;
    }

    void UpFundo()
    {
        if (seleAtualFundo == 0)
        {
            sFundo.sprite = obFundo[seleAtualFundo];
            bFundoPrevius.interactable = false;
        }
        else
            bFundoPrevius.interactable = true;

        if (seleAtualFundo > 0 && seleAtualFundo < obFundo.Length - 1)
        {
            sFundo.sprite = obFundo[seleAtualFundo];
            bFundoPrevius.interactable = true;
            bFundoNext.interactable = true;
        }
        if (seleAtualFundo >= obFundo.Length - 1)
        {
            sFundo.sprite = obFundo[seleAtualFundo];
            bFundoNext.interactable = false;
        }
        else
            bFundoNext.interactable = true;

    }

    public void NextFundo()
    {
        seleAtualFundo = seleAtualFundo + 1;

        if (obFundo[seleAtualFundo] == null)
            seleAtualFundo = seleAtualFundo + 1;
    }

    public void PreviusFundo()
    {
        seleAtualFundo = seleAtualFundo - 1;

        if (obFundo[seleAtualFundo] == null)
            seleAtualFundo = seleAtualFundo - 1;
    }

    void UpMascara()
    {
        if (seleAtualMascara == 0)
        {
            sMascara.sprite = obMascara[seleAtualMascara];
            bFundoPrevius.interactable = false;
        }
        else
            bFundoPrevius.interactable = true;

        if (seleAtualMascara > 0 && seleAtualMascara < obMascara.Length - 1)
        {
            sMascara.sprite = obMascara[seleAtualMascara];
            bFundoPrevius.interactable = true;
            bFundoNext.interactable = true;
        }
        if (seleAtualMascara >= obMascara.Length - 1)
        {
            sMascara.sprite = obMascara[seleAtualMascara];
            bFundoNext.interactable = false;
        }
        else
            bFundoNext.interactable = true;
    }

    public void NextMascara()
    {
        seleAtualMascara = seleAtualMascara + 1;

        if (obMascara[seleAtualMascara] == null)
            seleAtualMascara = seleAtualMascara + 1;
    }

    public void PreviusMascara()
    {
        seleAtualMascara = seleAtualMascara - 1;

        if (obMascara[seleAtualMascara] == null)
            seleAtualMascara = seleAtualMascara - 1;
    }

    public void Selecionado()
    {
        PlayerPrefs.SetString("pRigthtInstanciar", obPRigth[seleAtualPR].name);
        PlayerPrefs.SetString("pLeftInstanciar", obPLeft[seleAtualPL].name);
        PlayerPrefs.SetString("fundoInstanciar", obFundo[seleAtualFundo].name);
        PlayerPrefs.SetString("mascaraInstanciar", obMascara[seleAtualMascara].name);
        PlayerPrefs.SetString("goleiroInstanciar", obGoleiro[seleAtualGoleiro].name);
        
        PlayerPrefs.SetInt("seleAtualPR", seleAtualPR);
        PlayerPrefs.SetInt("seleAtualPL", seleAtualPL);
        PlayerPrefs.SetInt("seleAtualFundo", seleAtualFundo);
        PlayerPrefs.SetInt("seleAtualMascara", seleAtualMascara);
        PlayerPrefs.SetInt("seleAtualGoleiro", seleAtualGoleiro);

        Debug.Log(obFundo[seleAtualFundo].name);
        Debug.Log(obMascara[seleAtualMascara].name);
        Debug.Log(obPRigth[seleAtualPR].name);
        Debug.Log(obPLeft[seleAtualPL].name);
        Debug.Log(obGoleiro[seleAtualGoleiro].name);
    }
    public void LoadCena()
    {
        SceneManager.LoadScene("equip");
    }
    void AddSprite()
    {
        #region palheta
        if (PlayerPrefs.GetString("CompradoPalhetaFantasma") == "True")
        {
            obPLeft = new Sprite[obPLeft.Length + 1];
            obPRigth = new Sprite[obPRigth.Length + 1];

        }
        if (PlayerPrefs.GetString("CompradoPalhetaAmarela") == "True")
        {
            obPLeft = new Sprite[obPLeft.Length + 1];
            obPRigth = new Sprite[obPRigth.Length + 1];
        }
        if (PlayerPrefs.GetString("CompradoPalhetaRoxa") == "True")
        {
            obPLeft = new Sprite[obPLeft.Length + 1];
            obPRigth = new Sprite[obPRigth.Length + 1];
        }
        if (PlayerPrefs.GetString("CompradoPalhetaRosa") == "True")
        {
            obPLeft = new Sprite[obPLeft.Length + 1];
            obPRigth = new Sprite[obPRigth.Length + 1];
        }
        if (PlayerPrefs.GetString("CompradoPalhetaVerde") == "True")
        {
            obPLeft = new Sprite[obPLeft.Length + 1];
            obPRigth = new Sprite[obPRigth.Length + 1];
        }
        if (PlayerPrefs.GetString("CompradoPalhetaBolinhasAmarelo") == "True")
        {
            obPLeft = new Sprite[obPLeft.Length + 1];
            obPRigth = new Sprite[obPRigth.Length + 1];
        }
        if (PlayerPrefs.GetString("CompradoPalhetaBolinhasAzul") == "True")
        {
            obPLeft = new Sprite[obPLeft.Length + 1];
            obPRigth = new Sprite[obPRigth.Length + 1];
            Debug.Log("comprou palheta bolinha azul");
        }
        if (PlayerPrefs.GetString("CompradoPalhetaBolinhasVerde") == "True")
        {
            obPLeft = new Sprite[obPLeft.Length + 1];
            obPRigth = new Sprite[obPRigth.Length + 1];

        }
        if (PlayerPrefs.GetString("CompradoPalhetaBolinhasVermelho") == "True")
        {
            obPLeft = new Sprite[obPLeft.Length + 1];
            obPRigth = new Sprite[obPRigth.Length + 1];
        }

        if (PlayerPrefs.GetString("CompradoPalhetaFantasmaAzul") == "True")
        {
            obPLeft = new Sprite[obPLeft.Length + 1];
            obPRigth = new Sprite[obPRigth.Length + 1];
        }

        if (PlayerPrefs.GetString("CompradoPalhetaFantasmaVerde") == "True")
        {
            obPLeft = new Sprite[obPLeft.Length + 1];
            obPRigth = new Sprite[obPRigth.Length + 1];
        }

        if (PlayerPrefs.GetString("CompradoPalhetaFantasmaVermelho") == "True")
        {
            obPLeft = new Sprite[obPLeft.Length + 1];
            obPRigth = new Sprite[obPRigth.Length + 1];
        }
        if (PlayerPrefs.GetString("CompradoPalhetaGelo") == "True")
        {
            obPLeft = new Sprite[obPLeft.Length + 1];
            obPRigth = new Sprite[obPRigth.Length + 1];
        }
        if (PlayerPrefs.GetString("CompradoPalhetaPsiquicaVermelha") == "True")
        {
            obPLeft = new Sprite[obPLeft.Length + 1];
            obPRigth = new Sprite[obPRigth.Length + 1];
        }
        if (PlayerPrefs.GetString("CompradoPalhetaPsiquicaVerde") == "True")
        {
            obPLeft = new Sprite[obPLeft.Length + 1];
            obPRigth = new Sprite[obPRigth.Length + 1];
        }
        if (PlayerPrefs.GetString("CompradoPalhetaPsiquica") == "True")
        {
            obPLeft = new Sprite[obPLeft.Length + 1];
            obPRigth = new Sprite[obPRigth.Length + 1];
        }
        if (PlayerPrefs.GetString("CompradoPalhetaPsiquicaAzul") == "True")
        {
            obPLeft = new Sprite[obPLeft.Length + 1];
            obPRigth = new Sprite[obPRigth.Length + 1];
        }
        if (PlayerPrefs.GetString("CompradoPalhetaGeloAmarela") == "True")
        {
            obPLeft = new Sprite[obPLeft.Length + 1];
            obPRigth = new Sprite[obPRigth.Length + 1];
        }
        if (PlayerPrefs.GetString("CompradoPalhetaGeloRoxa") == "True")
        {
            obPLeft = new Sprite[obPLeft.Length + 1];
            obPRigth = new Sprite[obPRigth.Length + 1];
        }
        if (PlayerPrefs.GetString("CompradoPalhetaGeloVerde") == "True")
        {
            obPLeft = new Sprite[obPLeft.Length + 1];
            obPRigth = new Sprite[obPRigth.Length + 1];
        }
        #endregion

        #region Arena
        if (PlayerPrefs.GetString("CompradoFundoGelo") == "True")
        {
            obFundo = new Sprite[obFundo.Length + 1];
            obMascara = new Sprite[obMascara.Length + 1];
        }

        if (PlayerPrefs.GetString("CompradoFundoMadeira") == "True")
        {
            obFundo = new Sprite[obFundo.Length + 1];
            obMascara = new Sprite[obMascara.Length + 1];
        }
        if (PlayerPrefs.GetString("CompradoFundoMetal") == "True")
        {
            obFundo = new Sprite[obFundo.Length + 1];
            obMascara = new Sprite[obMascara.Length + 1];
        }
        if (PlayerPrefs.GetString("CompradoFundoAirHockey") == "True")
        {
            obFundo = new Sprite[obFundo.Length + 1];
            obMascara = new Sprite[obMascara.Length + 1];
        }
        if (PlayerPrefs.GetString("CompradoFundoAzulAmarelo") == "True")
        {
            obFundo = new Sprite[obFundo.Length + 1];
            obMascara = new Sprite[obMascara.Length + 1];
        }
        if (PlayerPrefs.GetString("CompradoFundoBlueCircles") == "True")
        {
            obFundo = new Sprite[obFundo.Length + 1];
            obMascara = new Sprite[obMascara.Length + 1];
        }
        if (PlayerPrefs.GetString("CompradoFundoFutebol") == "True")
        {
            obFundo = new Sprite[obFundo.Length + 1];
            obMascara = new Sprite[obMascara.Length + 1];
        }
        if (PlayerPrefs.GetString("CompradoFundoFutsal") == "True")
        {
            obFundo = new Sprite[obFundo.Length + 1];
            obMascara = new Sprite[obMascara.Length + 1];
        }
        if (PlayerPrefs.GetString("CompradoFundoPedra") == "True")
        {
            obFundo = new Sprite[obFundo.Length + 1];
            obMascara = new Sprite[obMascara.Length + 1];
        }
        if (PlayerPrefs.GetString("CompradoFundoRosa") == "True")
        {
            obFundo = new Sprite[obFundo.Length + 1];
            obMascara = new Sprite[obMascara.Length + 1];
        }
        if (PlayerPrefs.GetString("CompradoFundoTijolo") == "True")
        {
            obFundo = new Sprite[obFundo.Length + 1];
            obMascara = new Sprite[obMascara.Length + 1];
        }
        #endregion

        #region Goleiro
        if (PlayerPrefs.GetString("CompradoGoleiroAtaquePreto") == "True")
            obGoleiro = new Sprite[obGoleiro.Length + 1];

        if (PlayerPrefs.GetString("CompradoGoleiroRightPreto") == "True")
            obGoleiro = new Sprite[obGoleiro.Length + 1];

        if (PlayerPrefs.GetString("CompradoGoleiroBasicoVerde") == "True")
            obGoleiro = new Sprite[obGoleiro.Length + 1];

        if (PlayerPrefs.GetString("CompradoGoleiroLeftPreto") == "True")
            obGoleiro = new Sprite[obGoleiro.Length + 1];
        
        if (PlayerPrefs.GetString("CompradoGoleiroBasicoPreto") == "True")
            obGoleiro = new Sprite[obGoleiro.Length + 1];

        if (PlayerPrefs.GetString("CompradoGoleiroBasicoAmarelo") == "True")
            obGoleiro = new Sprite[obGoleiro.Length + 1];
        #endregion

        Load();
    }

    void Load()
    {
        #region Palhetas
        obPLeft[PlayerPrefs.GetInt("ValorArrayPalhetaFantasma") + 1] = Resources.Load<Sprite>("PalhetaFantasma");
        obPRigth[PlayerPrefs.GetInt("ValorArrayPalhetaFantasma") + 1] = Resources.Load<Sprite>("PalhetaFantasma");

        obPLeft[PlayerPrefs.GetInt("ValorArrayPalhetaAmarela") + 1] = Resources.Load<Sprite>("PalhetaAmarela");
        obPRigth[PlayerPrefs.GetInt("ValorArrayPalhetaAmarela") + 1] = Resources.Load<Sprite>("PalhetaAmarela");
        
        obPLeft[PlayerPrefs.GetInt("ValorArrayPalhetaRoxa") + 1] = Resources.Load<Sprite>("PalhetaRoxa");
        obPRigth[PlayerPrefs.GetInt("ValorArrayPalhetaRoxa") + 1] = Resources.Load<Sprite>("PalhetaRoxa");

        obPLeft[PlayerPrefs.GetInt("ValorArrayPalhetaRosa") + 1] = Resources.Load<Sprite>("PalhetaRosa");
        obPRigth[PlayerPrefs.GetInt("ValorArrayPalhetaRosa") + 1] = Resources.Load<Sprite>("PalhetaRosa");

        obPLeft[PlayerPrefs.GetInt("ValorArrayPalhetaVerde") + 1] = Resources.Load<Sprite>("PalhetaVerde");
        obPRigth[PlayerPrefs.GetInt("ValorArrayPalhetaVerde") + 1] = Resources.Load<Sprite>("PalhetaVerde");

        obPLeft[PlayerPrefs.GetInt("ValorArrayPalhetaBolinhasAmarelo") + 1] = Resources.Load<Sprite>("PalhetaBolinhasAmarelo");
        obPRigth[PlayerPrefs.GetInt("ValorArrayPalhetaBolinhasAmarelo") + 1] = Resources.Load<Sprite>("PalhetaBolinhasAmarelo");
        
        obPLeft[PlayerPrefs.GetInt("ValorArrayPalhetaBolinhasAzul") + 1] = Resources.Load<Sprite>("PalhetaBolinhasAzul");
        obPRigth[PlayerPrefs.GetInt("ValorArrayPalhetaBolinhasAzul") + 1] = Resources.Load<Sprite>("PalhetaBolinhasAzul");
        
        obPLeft[PlayerPrefs.GetInt("ValorArrayPalhetaBolinhasVerde") + 1] = Resources.Load<Sprite>("PalhetaBolinhasVerde");
        obPRigth[PlayerPrefs.GetInt("ValorArrayPalhetaBolinhasVerde") + 1] = Resources.Load<Sprite>("PalhetaBolinhasVerde");

        obPLeft[PlayerPrefs.GetInt("ValorArrayPalhetaBolinhasVermelho") + 1] = Resources.Load<Sprite>("PalhetaBolinhasVermelho");
        obPRigth[PlayerPrefs.GetInt("ValorArrayPalhetaBolinhasVermelho") + 1] = Resources.Load<Sprite>("PalhetaBolinhasVermelho");

        obPLeft[PlayerPrefs.GetInt("ValorArrayPalhetaFantasmaAzul") + 1] = Resources.Load<Sprite>("PalhetaFantasmaAzul");
        obPRigth[PlayerPrefs.GetInt("ValorArrayPalhetaFantasmaAzul") + 1] = Resources.Load<Sprite>("PalhetaFantasmaAzul");

        obPLeft[PlayerPrefs.GetInt("ValorArrayPalhetaFantasmaVerde") + 1] = Resources.Load<Sprite>("PalhetaFantasmaVerde");
        obPRigth[PlayerPrefs.GetInt("ValorArrayPalhetaFantasmaVerde") + 1] = Resources.Load<Sprite>("PalhetaFantasmaVerde");

        obPLeft[PlayerPrefs.GetInt("ValorArrayPalhetaFantasmaVermelho") + 1] = Resources.Load<Sprite>("PalhetaFantasmaVermelho");
        obPRigth[PlayerPrefs.GetInt("ValorArrayPalhetaFantasmaVermelho") + 1] = Resources.Load<Sprite>("PalhetaFantasmaVermelho");

        obPLeft[PlayerPrefs.GetInt("ValorArrayPalhetaGelo") + 1] = Resources.Load<Sprite>("PalhetaGelo");
        obPRigth[PlayerPrefs.GetInt("ValorArrayPalhetaGelo") + 1] = Resources.Load<Sprite>("PalhetaGelo");

        obPLeft[PlayerPrefs.GetInt("ValorArrayPalhetaPsiquicaAzul") + 1] = Resources.Load<Sprite>("PalhetaPsiquicaAzul");
        obPRigth[PlayerPrefs.GetInt("ValorArrayPalhetaPsiquicaAzul") + 1] = Resources.Load<Sprite>("PalhetaPsiquicaAzul");
        
        obPLeft[PlayerPrefs.GetInt("ValorArrayPalhetaPsiquica") + 1] = Resources.Load<Sprite>("PalhetaPsiquica");
        obPRigth[PlayerPrefs.GetInt("ValorArrayPalhetaPsiquica") + 1] = Resources.Load<Sprite>("PalhetaPsiquica");

        obPLeft[PlayerPrefs.GetInt("ValorArrayPalhetaPsiquicaVerde") + 1] = Resources.Load<Sprite>("PalhetaPsiquicaVerde");
        obPRigth[PlayerPrefs.GetInt("ValorArrayPalhetaPsiquicaVerde") + 1] = Resources.Load<Sprite>("PalhetaPsiquicaVerde");

        obPLeft[PlayerPrefs.GetInt("ValorArrayPalhetaPsiquicaVermelha") + 1] = Resources.Load<Sprite>("PalhetaPsiquicaVermelha");
        obPRigth[PlayerPrefs.GetInt("ValorArrayPalhetaPsiquicaVermelha") + 1] = Resources.Load<Sprite>("PalhetaPsiquicaVermelha");

        obPLeft[PlayerPrefs.GetInt("ValorArrayPalhetaGeloAmarela") + 1] = Resources.Load<Sprite>("PalhetaGeloAmarela");
        obPRigth[PlayerPrefs.GetInt("ValorArrayPalhetaGeloAmarela") + 1] = Resources.Load<Sprite>("PalhetaGeloAmarela");

        obPLeft[PlayerPrefs.GetInt("ValorArrayPalhetaGeloRoxa") + 1] = Resources.Load<Sprite>("PalhetaGeloRoxa");
        obPRigth[PlayerPrefs.GetInt("ValorArrayPalhetaGeloRoxa") + 1] = Resources.Load<Sprite>("PalhetaGeloRoxa");

        obPLeft[PlayerPrefs.GetInt("ValorArrayPalhetaGeloVerde") + 1] = Resources.Load<Sprite>("PalhetaGeloVerde");
        obPRigth[PlayerPrefs.GetInt("ValorArrayPalhetaGeloVerde") + 1] = Resources.Load<Sprite>("PalhetaGeloVerde");
        #endregion

        obFundo[PlayerPrefs.GetInt("ValorArrayFundoGelo")] = Resources.Load<Sprite>("FundoGelo");
        obFundo[PlayerPrefs.GetInt("ValorArrayFundoMadeira")] = Resources.Load<Sprite>("FundoMadeira");
        obFundo[PlayerPrefs.GetInt("ValorArrayFundoMetal")] = Resources.Load<Sprite>("FundoMetal");
        obFundo[PlayerPrefs.GetInt("ValorArrayFundoAirHockey")] = Resources.Load<Sprite>("FundoAirHockey");
        obFundo[PlayerPrefs.GetInt("ValorArrayFundoAzulAmarelo")] = Resources.Load<Sprite>("FundoAzulAmarelo");
        obFundo[PlayerPrefs.GetInt("ValorArrayFundoBlueCircles")] = Resources.Load<Sprite>("FundoBlueCircles");
        obFundo[PlayerPrefs.GetInt("ValorArrayFundoFutebol")] = Resources.Load<Sprite>("FundoFutebol");
        obFundo[PlayerPrefs.GetInt("ValorArrayFundoFutsal")] = Resources.Load<Sprite>("FundoFutsal");
        obFundo[PlayerPrefs.GetInt("ValorArrayFundoPedra")] = Resources.Load<Sprite>("FundoPedra");
        obFundo[PlayerPrefs.GetInt("ValorArrayFundoRosa")] = Resources.Load<Sprite>("FundoRosa");
        obFundo[PlayerPrefs.GetInt("ValorArrayFundoTijolo")] = Resources.Load<Sprite>("FundoTijolo");

        obMascara[PlayerPrefs.GetInt("ValorArrayFundoGelo")] = Resources.Load<Sprite>("MascaraGelo");
        obMascara[PlayerPrefs.GetInt("ValorArrayFundoMadeira")] = Resources.Load<Sprite>("MascaraMadeira");
        obMascara[PlayerPrefs.GetInt("ValorArrayFundoMetal")] = Resources.Load<Sprite>("MascaraMetal");
        obMascara[PlayerPrefs.GetInt("ValorArrayFundoAirHockey")] = Resources.Load<Sprite>("MascaraAirHockey");
        obMascara[PlayerPrefs.GetInt("ValorArrayFundoAzulAmarelo")] = Resources.Load<Sprite>("MascaraAzulAmarelo");
        obMascara[PlayerPrefs.GetInt("ValorArrayFundoBlueCircles")] = Resources.Load<Sprite>("MascaraBlueCircles");
        obMascara[PlayerPrefs.GetInt("ValorArrayFundoFutebol")] = Resources.Load<Sprite>("MascaraFutebol");
        obMascara[PlayerPrefs.GetInt("ValorArrayFundoFutsal")] = Resources.Load<Sprite>("MascaraFutsal");
        obMascara[PlayerPrefs.GetInt("ValorArrayFundoPedra")] = Resources.Load<Sprite>("MascaraPedra");
        obMascara[PlayerPrefs.GetInt("ValorArrayFundoRosa")] = Resources.Load<Sprite>("MascaraRosa");
        obMascara[PlayerPrefs.GetInt("ValorArrayFundoTijolo")] = Resources.Load<Sprite>("MascaraTijolo");



        obGoleiro[PlayerPrefs.GetInt("ValorArrayGoleiroLeftPreto") + 1] = Resources.Load<Sprite>("GoleiroLeftPreto");
        obGoleiro[PlayerPrefs.GetInt("ValorArrayGoleiroRightPreto") + 1] = Resources.Load<Sprite>("GoleiroRightPreto");
        obGoleiro[PlayerPrefs.GetInt("ValorArrayGoleiroBasicoVerde") + 1] = Resources.Load<Sprite>("GoleiroBasicoVerde");
        obGoleiro[PlayerPrefs.GetInt("ValorArrayGoleiroAtaquePreto") + 1] = Resources.Load<Sprite>("GoleiroAtaquePreto");
        obGoleiro[PlayerPrefs.GetInt("ValorArrayGoleiroBasicoPreto") + 1] = Resources.Load<Sprite>("GoleiroBasicoPreto");
        obGoleiro[PlayerPrefs.GetInt("ValorArrayGoleiroBasicoAmarelo")+ 1] = Resources.Load<Sprite>("GoleiroBasicoAmarelo");
    }
}

