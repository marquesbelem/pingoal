﻿using UnityEngine;

public class PalhetaAnulaPoder : MonoBehaviour {

    public GameObject camadaGeloPrefab;
    public bool colediu;
    private GameObject ball;
    private GameObject camadaGeloOficial;
    private static bool instaciBall = false;
	void Start () {

        ball = GameObject.FindGameObjectWithTag("Bola");
       
       camadaGeloOficial = Instantiate(camadaGeloPrefab, ball.transform);
       camadaGeloOficial.transform.SetParent(ball.transform);


        VerificaLadoPalheta();
    }
    
    void OnCollisionEnter2D(Collision2D coll)
    {
        if (coll.gameObject.tag == "Bola" && colediu == false)
        {
            CamadaDeGelo(true);
            DesabilitaPalhetas("CamadaGelo");
        }
            

    }

   public void CamadaDeGelo(bool v)
    {
        camadaGeloOficial.SetActive(v);
        colediu = v;
    }

    public bool ladoPalhetaEmBaixo;
    public bool ladoPalhetaEmCima;
    void VerificaLadoPalheta()
    {
        if (this.transform.position.y <= 0)
            ladoPalhetaEmBaixo = true;
        else
            ladoPalhetaEmCima = true;

    }

    public void DesabilitaPalhetas(string tag)
    {
        ball.tag = tag;
    }
}
