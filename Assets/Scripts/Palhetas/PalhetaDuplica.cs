﻿using UnityEngine;

public class PalhetaDuplica : MonoBehaviour {

    public GameObject ball;
    public static bool bolaDuplaAtiva;
    public GameObject ballDup;

    void Start()
    {
        bolaDuplaAtiva = false;
    }
    void Update () {

        
        if (BallContagem.contagem >= 20 && ballDup != null)
        { 
            PararBola();
            BallContagem.contagem = 0;
        }

        //seta cor 
        if(ballDup != null && BallContagem.contagem == 18)
        {
            Color alfaC = ballDup.GetComponent<SpriteRenderer>().color;
            alfaC = Color.red;
            ballDup.GetComponent<SpriteRenderer>().color = alfaC;
        }


        VerificaBall();
    }
    
    void OnCollisionEnter2D(Collision2D coll)
    {
        if (coll.gameObject.tag == "Bola" && ballDup == null && bolaDuplaAtiva == false)
        {
            GameObject g = GameObject.FindGameObjectWithTag("Bola");
            Vector3 t = new Vector3(g.transform.position.x, g.transform.position.y + 2, g.transform.position.z);
            ballDup = Instantiate(ball, t, Quaternion.identity);
            bolaDuplaAtiva = true; 


        }
        if (coll.gameObject.tag == "Bola" && ballDup != null && bolaDuplaAtiva == false)
        {
            GameObject g = GameObject.FindGameObjectWithTag("Bola");
            Vector3 t = new Vector3(g.transform.position.x, g.transform.position.y + 2, g.transform.position.z);
            ballDup.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.None;
            ballDup.transform.position = t;
            bolaDuplaAtiva = true;
        }
    }

    private GameObject[] g;

    void VerificaBall()
    {
        g = GameObject.FindGameObjectsWithTag("BolaDupla");

        if (g.Length >= 2)
        {
            Destroy(g[0]);
        }
    }

    void PararBola()
    {
        ballDup.transform.position = new Vector3(-7.38f, 0f, 0f);
        ballDup.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeAll;

        Color32 alfaC = ballDup.GetComponent<SpriteRenderer>().color;
        alfaC.r = 54;
        alfaC.g = 46;
        alfaC.b = 46;
        ballDup.GetComponent<SpriteRenderer>().color = alfaC;

        bolaDuplaAtiva = false;
    }
}
