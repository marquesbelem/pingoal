﻿using UnityEngine;

public class PalhetaBolaInvi : MonoBehaviour {

    public GameObject ball;
    private bool setTime;
    private float time = 0.5f;

    void Start()
    {
        ball = GameObject.FindGameObjectWithTag("Bola");
    }
    void Update()
    {
        if (setTime)
            TimeInv();
    }
    void TimeInv()
    {
        time -= Time.deltaTime; 

        if(time < 0)
        {
            setTime = false;
            InviBall(false);
            time = 0.5f;
        }
    }

    void InviBall(bool inv)
    {
        if (inv)
        {
            Color c = ball.GetComponent<SpriteRenderer>().color;
            c.a = 0f;
            ball.GetComponent<SpriteRenderer>().color = c;
        }
        if(inv == false){
            Color c = ball.GetComponent<SpriteRenderer>().color;
            c.a = 255f;
            ball.GetComponent<SpriteRenderer>().color = c;
        }
    }

    void OnCollisionEnter2D(Collision2D coll)
    {
        if (coll.gameObject.tag == "Bola")
        {
            setTime = true;
            InviBall(true);
        }
    }
    
}
