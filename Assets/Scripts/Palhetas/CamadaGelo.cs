﻿using UnityEngine;

public class CamadaGelo : MonoBehaviour
{

    private PalhetaAnulaPoder palhetaGelo;
    private int contadorColisao;
    void Start()
    {
        palhetaGelo = FindObjectOfType<PalhetaAnulaPoder>();

    }

    void Update()
    {
         if(contadorColisao >= 20)
         {
             palhetaGelo.CamadaDeGelo(false);
             palhetaGelo.DesabilitaPalhetas("Bola");
             contadorColisao = 0;
         }
        
    }

    void OnTriggerEnter2D(Collider2D coll)
    {

        if (coll.gameObject.tag == "BotLeft" || coll.gameObject.tag == "BotRigth"
            || coll.gameObject.tag == "Palheta"
            && palhetaGelo.colediu)
        {
            if (palhetaGelo.ladoPalhetaEmBaixo)
            {
                if (coll.gameObject.transform.position.y >= 5)
                    palhetaGelo.DesabilitaPalhetas("CamadaGelo");
                else
                    palhetaGelo.DesabilitaPalhetas("Bola");

            }
            else if (palhetaGelo.ladoPalhetaEmCima)
            {
                if (coll.gameObject.transform.position.y <= -5)
                    palhetaGelo.DesabilitaPalhetas("CamadaGelo");
                else
                    palhetaGelo.DesabilitaPalhetas("Bola");
            }
        }



        if (coll.gameObject.tag == "BotLeft"
            || coll.gameObject.tag == "BotRigth"
            || coll.gameObject.tag == "Palheta"
            || coll.gameObject.tag == "GoleiroPlayer"
            || coll.gameObject.tag == "GoleiroBot"
            || coll.gameObject.tag == "Arena")
        {
            contadorColisao += 1;
        }

    }





}
