﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement; 

public class PassaLevel : MonoBehaviour {

	public string proximoLevel; 

	void Start () {
		Invoke("CarregaCena",2);
	}
	
	void CarregaCena()
	{
		VariaveisTemp.proximoNivel = proximoLevel;
		SceneManager.LoadScene ("Loading");
	}
}
