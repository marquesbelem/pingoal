﻿using UnityEngine;

public class GoalkeeperBot : MonoBehaviour
{

    public static GameObject ball;
    public Transform max, min;
    public float force;
    public static float speed = 0.2f;

    void FixedUpdate()
    {
        MoveGoalkeeper();

        
    }

    void MoveGoalkeeper()
    {
        ball = GameObject.FindGameObjectWithTag("Bola");
        ballDupla = GameObject.FindGameObjectWithTag("BolaDupla");

        if (speed > 0.2f)
            this.transform.position = new Vector2(Mathf.Clamp(ball.transform.position.x * speed, -1,  2), transform.position.y);
        else
            this.transform.position = new Vector2(Mathf.Clamp(ball.transform.position.x * speed, - 2.5f, 3.5f), transform.position.y);
    }
    
    void OnCollisionEnter2D(Collision2D coll)
    {
        if (coll.gameObject.tag == "Bola" )
            Thrust(true);
        if (coll.gameObject.tag == "BolaDupla")
            Thrust(false);
    }

    private  GameObject ballDupla;
    //Impulso pra palheta que tiver em baixo
    public void Thrust(bool b)
    {
        Vector3 pos = new Vector3(-1.9f, transform.position.y, transform.position.z);
        if(b)
            ball.GetComponent<Rigidbody2D>().AddForce(pos * force);
        else
        {
           // if (ballDupla != null)
                ballDupla.GetComponent<Rigidbody2D>().AddForce(pos * force);
        }
    }
   
}
