﻿using UnityEngine;

public class PequenoGrandeGoleiro : MonoBehaviour
{

    public bool colediu;
    private Vector3 tamanhoNormal;
    public Vector3 tamanhoGrande;
    private float time;
    void Start()
    {
        tamanhoNormal = this.transform.localScale;
    }

    void Update()
    {

        time += Time.deltaTime;
        if (time > 4)
            PequenoGrande(tamanhoGrande);
        if (time > 8)
        {
            PequenoGrande(tamanhoNormal);
            time = 0;
        }


    }
    void OnCollisionEnter2D(Collision2D coll)
    {
        if (coll.gameObject.tag == "Bola")
        {

            colediu = true;
        }

    }

    void PequenoGrande(Vector3 tamanho)
    {
        this.transform.localScale = tamanho;
    }


}
