﻿using UnityEngine;
public class GoalkeeperUnet : MonoBehaviour
{

    private static GameObject ball;
    public static float speed = 2f;
    private Rigidbody2D rig;
    private GameObject ballDupla;
    public float force;

    public enum TipoDeImpulso
    {
        Left,
        Right,
        ProGol,
        Normal
    };

    public TipoDeImpulso impulsos;

    TipoDeImpulso Tipos(TipoDeImpulso imp, bool b)
    {
        if (imp == TipoDeImpulso.Left)
            Left(b);
        else if (imp == TipoDeImpulso.Right)
            Rigth(b);
        else if (imp == TipoDeImpulso.ProGol)
            ProGol(b);
        else if (imp == TipoDeImpulso.Normal)
            Normal(b);


        return imp;
    }
    void Start()
    {
        if (transform.position.y >= 0)
        {
            this.transform.position = new Vector2(this.transform.position.x, 5.95f);
        }
    
        rig = GetComponent<Rigidbody2D>();
    }
    void FixedUpdate()
    {
        MoveGoalkeeper();
    }

    void MoveGoalkeeper()
    {
        ball = GameObject.FindGameObjectWithTag("Bola");
            if(ball != null)
                 rig.MovePosition(ball.transform.position * speed * Time.deltaTime);
    }
   
    void OnCollisionEnter2D(Collision2D coll)
    {
        if (coll.gameObject.tag == "Bola")
            Tipos(impulsos, true);
        if (coll.gameObject.tag == "BolaDupla")
        {
            ballDupla = GameObject.FindGameObjectWithTag("BolaDupla");
            Tipos(impulsos, false);

        }
    }

    void Left(bool b)
    {
        if (b)
            ball.GetComponent<Rigidbody2D>().velocity = new  Vector2(-3f,-ball.transform.position.y * force);
        else
            ballDupla.GetComponent<Rigidbody2D>().velocity = new Vector2(-3f, -ballDupla.transform.position.y * force); 

    }

    void Rigth(bool b)
    {
        if (b)
            ball.GetComponent<Rigidbody2D>().velocity = new Vector2(3f, -ball.transform.position.y * force);
        else
            ballDupla.GetComponent<Rigidbody2D>().velocity = new Vector2(3f, -ballDupla.transform.position.y * force);
    }

    void ProGol(bool b)
    {
        float x = Random.Range(-0.3f, 1);
        if (b)
            ball.GetComponent<Rigidbody2D>().velocity = new Vector2(x, -ball.transform.position.y * force);
        else
            ballDupla.GetComponent<Rigidbody2D>().velocity = new Vector2(x, -ballDupla.transform.position.y * force);
    }

    void Normal(bool b)
    {
        if (b)
            ball.GetComponent<Rigidbody2D>().AddForce(new Vector2(ball.transform.position.x * 2f, -ball.transform.position.y) * force);
        else
            ballDupla.GetComponent<Rigidbody2D>().AddForce(new Vector2(ballDupla.transform.position.x * 2f, -ballDupla.transform.position.y) * force);

    }


}