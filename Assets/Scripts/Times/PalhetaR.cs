﻿using UnityEngine;

public class PalhetaR : MonoBehaviour {

    public Rigidbody2D rightFlipperRigid;
    public float torqueForce;
    public bool player;
    void Start()
    {
        rightFlipperRigid = this.GetComponent<Rigidbody2D>();
    }

    void FixedUpdate()
    {
        if (player)
        {
            if (Input.GetMouseButtonDown(0))
            {
                Vector3 mouseInput = Input.mousePosition;
                if (mouseInput.x >= Screen.width / 2f)
                {
                    RightFlipper1();
                }
            }
            else if (Input.GetMouseButton(0))
            {
                Vector3 mouseHolding = Input.mousePosition;
                if (mouseHolding.x >= Screen.width / 2f)
                {
                    RightFlipper1();
                }
            }
        }
    }

    public void RightFlipper1()
    {
        AddTorque(rightFlipperRigid, -torqueForce);
    }

    void AddTorque(Rigidbody2D rigid, float force)
    {
        rigid.AddTorque(force);
    }
}
