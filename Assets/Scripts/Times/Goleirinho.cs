﻿using UnityEngine;

public class Goleirinho : MonoBehaviour {

    private Game game;
    private float distanceZ;
    private float bottomBorder;
    private float topBorder;
    public bool colediu;
    private GameObject ball; 
    void Start()
    {
        ball = GameObject.FindGameObjectWithTag("Bola");
        game = FindObjectOfType<Game>();
        distanceZ = (transform.position - Camera.main.transform.position).z;
        bottomBorder = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, distanceZ)).y;
        topBorder = Camera.main.ViewportToWorldPoint(new Vector3(0, 1, distanceZ)).y;
    }
    void Update()
    {
        if(colediu == true && ball.transform.position.y > topBorder + 10)
        {
            game.pPlayer += 1;
            colediu = false;
        }
        if (colediu == true && ball.transform.position.y < bottomBorder - 10)
        {
            game.pBot += 1;
            colediu = false;
        }
    }

    void OnCollisionEnter2D(Collision2D coll)
    {
        if (coll.gameObject.tag == "Bola")
            colediu = true;
    }

}
