﻿using UnityEngine;

public class PalhetaL : MonoBehaviour
{
    public Rigidbody2D leftFlipperRigid;
    public float torqueForce;
    public bool player;
    void Start()
    {
        leftFlipperRigid = this.GetComponent<Rigidbody2D>();
    }
    void FixedUpdate()
    {
        if (player)
        {
            if (Input.GetMouseButtonDown(0))
            {
                Vector3 mouseInput = Input.mousePosition;
                if (mouseInput.x < Screen.width / 2f)
                {
                    LeftFlipper1();
                }
                
            }
            else if (Input.GetMouseButton(0))
            {
                Vector3 mouseHolding = Input.mousePosition;
                if (mouseHolding.x < Screen.width / 2f)
                {
                    LeftFlipper1();
                }
                
            }
        }
        
    }

    public void LeftFlipper1()
    {
        AddTorque(leftFlipperRigid, torqueForce);
    }
    void AddTorque(Rigidbody2D rigid, float force)
    {
        rigid.AddTorque(force);
    }

    
}