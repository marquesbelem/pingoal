﻿using UnityEngine.UI;
using UnityEngine;

public class ShopHandle : MonoBehaviour {

    //Conter as mesmas informação do itemScript
    [System.Serializable]
    public class Item
    {
        public Sprite image;
        public int valor;
        public string name;
        public string textInformativo;
        public string tipo;
        public string textDescricaoPanelConfirm;
    }

    public Item[] shopItems;

    //Item feito pra ser "clonado"
    public GameObject item;


   
  
    GameObject it;

    void Start()
    {
        foreach (Item i in shopItems)
        {

            it = (GameObject)Instantiate(item);


            ItemScript itemScript = it.GetComponent<ItemScript>();

            itemScript.valorText.text = i.valor.ToString();
            itemScript.icon.sprite = i.image;
            itemScript.iconPanelConfirm.sprite = i.image;
            itemScript.valor = i.valor;
            itemScript.textInformativo.text = i.textInformativo;
            itemScript.textNomePanel.text = i.textInformativo.Replace(" ","");
            
            itemScript.textDescricaoPanel.text = i.textDescricaoPanelConfirm;
            itemScript.tipo = i.tipo;
            it.name = i.name;
            it.transform.SetParent(this.gameObject.transform);
            it.transform.localScale = new Vector3(1f, 1f, 1f);
            it.transform.position = this.gameObject.transform.position;


        }

    }

}

