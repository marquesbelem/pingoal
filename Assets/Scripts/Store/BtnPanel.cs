﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class BtnPanel : MonoBehaviour {

    public GameObject [] panel;
    public Button btnStartPalheta;
    public Button btnStartGoleiro;
    public Button btnStartArena;

 
	
    public void TrocaPanel(string nomeBtn)
    {
        foreach (GameObject g in panel)
        {
            if (g.name == "Palhetas" && nomeBtn == "Palhetas")
            {
                g.SetActive(true);
                
            }
            else
            {
                g.SetActive(false);
            }
            if (g.name == "Goleiros" && nomeBtn == "Goleiros")
            {
                g.SetActive(true);
                
            }
            if (g.name == "Arenas" && nomeBtn == "Arenas")
            {

                g.SetActive(true);
            }
        

        }
        
    }


    void SelectCorButn(Button btn)
    {
        ColorBlock cb = btn.colors;
        cb.normalColor = Color.red;
        btn.colors = cb;
    }

    void DeselectCorButn(Button btn)
    {
        ColorBlock cb = btn.colors;
        cb.normalColor = Color.white;
        btn.colors = cb;
    }

}
