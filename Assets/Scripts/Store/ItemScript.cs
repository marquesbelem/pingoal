﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class ItemScript : MonoBehaviour
{

    public Image icon;
    public int valor;
    public Text valorText;
    public GameObject btnBuy;
    public bool comprado;
    public Text textInformativo;


    public Image iconPanelConfirm;
    public Text textNomePanel;
    public Text textDescricaoPanel;

    public GameObject panelConfirm;
    public GameObject canvas;
    public Text valorPanel;
    public string tipo;
    public GameObject caixaSelect;
    void Start()
    {
        canvas = GameObject.Find("Canvas");

    }
    void Update()
    {
        VerificaValor();

        valorArrayPalheta = PlayerPrefs.GetInt("ValorArrayTotalPalheta");
        valorArrayFundo = PlayerPrefs.GetInt("ValorArrayTotalFundo");
        valorArrayMascara = PlayerPrefs.GetInt("ValorArrayTotalMascara");
        valorArrayGoleiro = PlayerPrefs.GetInt("ValorArrayTotalGoleiro");
        if (PlayerPrefs.GetString("Comprado" + gameObject.name) == "True")
        {
            btnBuy.GetComponent<Button>().interactable = false;
            caixaSelect.SetActive(true);
        }
            
    }

    void VerificaValor()
    {
        if (valor > PlayerPrefs.GetInt("ApostaTotal"))
            btnBuy.GetComponent<Button>().interactable = false;
        else
            btnBuy.GetComponent<Button>().interactable = true;
    }

    int valorArrayPalheta;
    int valorArrayFundo;
    int valorArrayMascara;
    int valorArrayGoleiro;
    public void Comprar()
    {
        int valorTotal = PlayerPrefs.GetInt("ApostaTotal");

        valorTotal -= valor; 

        if (tipo == "Palheta" || tipo == "palheta")
        {
            valorArrayPalheta += 1;
            comprado = true;
            string c = comprado.ToString();
            PlayerPrefs.SetString("Comprado" + gameObject.name, c);
            PlayerPrefs.SetInt("ValorArray" + gameObject.name, valorArrayPalheta);
            PlayerPrefs.SetInt("ValorArrayTotalPalheta", valorArrayPalheta);
        }

        if (tipo == "Arena" || tipo == "arena")
        {
            valorArrayFundo += 1;
            valorArrayMascara += 1;
            comprado = true;
            string c = comprado.ToString();
            PlayerPrefs.SetString("Comprado" + gameObject.name, c);
            PlayerPrefs.SetInt("ValorArray" + gameObject.name, valorArrayFundo);
            PlayerPrefs.SetInt("ValorArrayTotalFundo", valorArrayFundo);
            
        }
        if (tipo == "Goleiro" || tipo == "goleiro")
        {
            valorArrayGoleiro += 1;
            comprado = true;
            string c = comprado.ToString();
            PlayerPrefs.SetString("Comprado" + gameObject.name, c);
            PlayerPrefs.SetInt("ValorArray" + gameObject.name, valorArrayGoleiro);
            PlayerPrefs.SetInt("ValorArrayTotalGoleiro", valorArrayGoleiro);

        }

        FecharPanelConfirm();
        PlayerPrefs.SetInt("ApostaTotal", valorTotal);
        Debug.Log(PlayerPrefs.GetString("Comprado" + gameObject.name));

    }


    public void AbrirPanelConfirm()
    {
        panelConfirm.SetActive(true);
        panelConfirm.transform.SetParent(canvas.transform);
        panelConfirm.transform.position = canvas.transform.position;
        panelConfirm.transform.localScale = new Vector3(18f, 22f, 5f);
        valorPanel.GetComponent<Text>().text = valor.ToString();
    }
    
    public void FecharPanelConfirm()
    {
        panelConfirm.SetActive(false);
        panelConfirm.transform.SetParent(this.gameObject.transform);
        panelConfirm.transform.position = this.gameObject.transform.position;
    }

}
