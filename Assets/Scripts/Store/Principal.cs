﻿using UnityEngine.UI;
using UnityEngine;

using UnityEngine.SceneManagement;
public class Principal : MonoBehaviour {

    public Text moedas;

	void Update () {
		moedas.text = PlayerPrefs.GetInt("ApostaTotal").ToString();
    }


    public void Replay(string scene)
    {
        SceneManager.LoadScene(scene);
    }
    

}
