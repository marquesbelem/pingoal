﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NavCtrl1 : MonoBehaviour {

    private string scene;
    private Fade fade;
    private AniBtnMenu aniBtn;
    void Start()
    {
        fade = FindObjectOfType<Fade>();
        aniBtn = FindObjectOfType<AniBtnMenu>();
    }
    public void Play(string s)
    {
        aniBtn.AtiveAni();
        scene = s; 
        StartCoroutine("TimePlay");
        
    }

   
    public void Replay(string scene)
    {
        SceneManager.LoadScene(scene);
    }

    IEnumerator TimePlay()
    {
        yield return new WaitForSeconds(1.8f);
        float fadeTime = fade.BeginFade(1);
        yield return new WaitForSeconds(fadeTime);
        SceneManager.LoadScene(scene);
    }
}
