﻿#if UNITY_ANDROID
using UnityEngine;
using UnityEngine.UI;

public class Aposta : MonoBehaviour
{

    public Text apostaTotal;
    public int moedasP;
    public int setMoedas;
    private ADSUnity ads;
    void Start()
    {
        ads = FindObjectOfType<ADSUnity>();
        setMoedas = PlayerPrefs.GetInt("setMoedas");

        if (setMoedas == 0)
            PlayerPrefs.SetInt("ApostaTotal", moedasP);

        moedasP = PlayerPrefs.GetInt("ApostaTotal");

        setMoedas = 1;

        PlayerPrefs.SetInt("setMoedas", setMoedas);

        if (moedasP <= 0)
        {
            moedasP = 0;
            PlayerPrefs.SetInt("ApostaTotal", moedasP);
        }

    }


    void Update()
    {
        moedasP = PlayerPrefs.GetInt("ApostaTotal");

        apostaTotal.text = moedasP.ToString();
    }
    
    public void Apostar(int value)
    {
        moedasP -= value;
        PlayerPrefs.SetInt("ApostaTotal", moedasP);
        PlayerPrefs.SetInt("ValorAposta", value);
        
    }

}
#endif