﻿
using UnityEngine;
using UnityEngine.UI;

public class ApostaGame : MonoBehaviour {

    public Text valorApoTotal;
    private int valorTotal;

    private int apostaBot = 100;
    private int moedasPlayer; 

    void Start () {
        moedasPlayer = PlayerPrefs.GetInt("valorDaAposta");
        valorTotal = moedasPlayer + apostaBot;
        valorApoTotal.text = valorTotal.ToString();
    }
    public void VitoriaPlayer()
    {
        moedasPlayer = PlayerPrefs.GetInt("ApostaTotal");
        moedasPlayer += valorTotal;
        PlayerPrefs.SetInt("ApostaTotal", moedasPlayer);
    }

    public void DerrotaPlayer()
    {
        moedasPlayer = PlayerPrefs.GetInt("ApostaTotal");
    }
}
