﻿using System.Collections;
using UnityEngine;
public class BallMenu : MonoBehaviour
{

    public static float massB;
    public bool bolaDefault;
    private Rigidbody2D rig;
    public GameObject exit;
    public static float rotaValue = 0;

    void Start()
    {
        exit = GameObject.FindGameObjectWithTag("ExitLeft");
        rig = GetComponent<Rigidbody2D>();

        if (!bolaDefault)
        {
            Color32 alfaC = this.GetComponent<SpriteRenderer>().color;
            alfaC.r = 54;
            alfaC.g = 46;
            alfaC.b = 46;
            this.GetComponent<SpriteRenderer>().color = alfaC;
        }
    }
    void Update()
    {
        rig.AddTorque(transform.rotation.z * rotaValue * Time.deltaTime, ForceMode2D.Impulse);
        GravityBall();

        float distanceZ = (transform.position - Camera.main.transform.position).z;
        float bottomBorder = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, distanceZ)).y;
        float topBorder = Camera.main.ViewportToWorldPoint(new Vector3(0, 1, distanceZ)).y;

        if (transform.position.y > topBorder + 10)
        {
            if (bolaDefault)
                this.transform.position = exit.transform.position;
            else
            {
                BolaDuplaForaCena();
                BallContagem.contagem = 0;
            }

        }
        if (transform.position.y < bottomBorder - 10)
        {
            if (bolaDefault)
                this.transform.position = exit.transform.position;
            else
            {

                BolaDuplaForaCena();
                BallContagem.contagem = 0;
            }
        }

    }
    void BolaDuplaForaCena()
    {
        this.gameObject.transform.position = new Vector3(-7.38f, 0f, 0f);
        rig.constraints = RigidbodyConstraints2D.FreezeAll;


        //cor padrão 
        if (!bolaDefault)
        {
            Color32 alfaC = this.GetComponent<SpriteRenderer>().color;
            alfaC.r = 54;
            alfaC.g = 46;
            alfaC.b = 46;
            this.GetComponent<SpriteRenderer>().color = alfaC;
        }

        PalhetaDuplica.bolaDuplaAtiva = false;
    }

    void GravityBall()
    {
        if (this.transform.position.y > 0)
            GetComponent<Rigidbody2D>().gravityScale = -GetComponent<Rigidbody2D>().mass - (-1);
        if (this.transform.position.y < 0)
            GetComponent<Rigidbody2D>().gravityScale = GetComponent<Rigidbody2D>().mass - 1;
    }



}
