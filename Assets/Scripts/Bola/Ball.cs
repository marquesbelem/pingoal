﻿using UnityEngine;
public class Ball : MonoBehaviour
{

    public static float massB;
    public static float rotaValue = 0;
    private Game game;
    public bool bolaDefault;
    private Rigidbody2D rig;
    private Goleirinho golerinho;
    void Start()
    {
        game = FindObjectOfType<Game>();
        rig = GetComponent<Rigidbody2D>();

    }

    void Update()
    {

        GravityBall();
        rig.AddTorque(transform.rotation.z * rotaValue * Time.deltaTime, ForceMode2D.Impulse);
    }

    void GravityBall()
    {
        if (this.transform.position.y > 0)
            rig.gravityScale = -rig.mass - (-1);
        if (this.transform.position.y < 0)
            rig.gravityScale = rig.mass - 1;
    }

    void OnCollisionEnter2D(Collision2D coll)
    {
        if (coll.gameObject.tag == "Palheta" && golerinho != null)
        {
            golerinho = FindObjectOfType<Goleirinho>();
            golerinho.colediu = false;
        }
        
    }
  

    void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.gameObject.tag == "jogadorDeBaixo")
        {
            Debug.Log("colediu");
            if (bolaDefault)
            {
                game.pPlayer += 1;
                Game.ladoEmY = -1;
                Game.ladoEmX = 1;
                //Game.setBall = true;
                game.Pingooal();

            }
            else
            {
                game.pPlayer += 1;
                BolaDuplaForaCena();
                BallContagem.contagem = 0;
            }
        }
        if (coll.gameObject.tag == "jogadorDeCima")
        {
            Debug.Log("colediu");
            if (bolaDefault)
            {
                game.pBot += 1;
                Game.ladoEmY = 1;
                Game.ladoEmX = 0;
                //Game.setBall = true;
                game.Pingooal();
            }
            else
            {
                game.pBot += 1;
                BolaDuplaForaCena();
                BallContagem.contagem = 0;
            }
        }
    }
    public void BolaDuplaForaCena()
    {
        this.gameObject.transform.position = new Vector3(-7.38f, 0f, 0f);
        rig.constraints = RigidbodyConstraints2D.FreezeAll;


        //cor padrão 
        if (!bolaDefault)
        {
            Color32 alfaC = this.GetComponent<SpriteRenderer>().color;
            alfaC.r = 54;
            alfaC.g = 46;
            alfaC.b = 46;
            this.GetComponent<SpriteRenderer>().color = alfaC;
        }

        PalhetaDuplica.bolaDuplaAtiva = false;
    }

    public void ColocaAlfa(byte num)
    {
        Color32 alfaC = this.GetComponent<SpriteRenderer>().color;
        alfaC.a -= num;
        this.GetComponent<SpriteRenderer>().color = alfaC;
    }

    public void TiraAlfa()
    {
        Color32 alfaC = this.GetComponent<SpriteRenderer>().color;
        alfaC.a = 255;
        this.GetComponent<SpriteRenderer>().color = alfaC;
    }
}

