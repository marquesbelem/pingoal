﻿using UnityEngine;

public class BallContagem : MonoBehaviour
{
    public static int contagem;
    void OnCollisionEnter2D(Collision2D coll)
    {
        if (coll.gameObject.tag == "BotLeft" || coll.gameObject.tag == "BotRigth" 
            || coll.gameObject.tag == "Palheta" || coll.gameObject.tag == "Arena"
            || coll.gameObject.tag == "GoleiroPlayer" || coll.gameObject.tag == "GoleiroBot")
        {
            contagem += 1;
        }
    }
}