﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Game : MonoBehaviour
{
    private PalhetaAnulaPoder palhetaGelo;
    public float time;
    public bool setTime;
    public int pPlayer;
    public int pBot;
    private GameObject ball;
    public GameObject exitLeft;
    public GameObject exitRigth;
    public static float ladoEmY;
    public static float ladoEmX;
    public static bool setBall;
    private float ladoExit;
    public bool partidaExecutePanel;
    void Start()
    {
        palhetaGelo = FindObjectOfType<PalhetaAnulaPoder>();
        exitLeft = GameObject.FindGameObjectWithTag("ExitLeft");
        exitRigth = GameObject.FindGameObjectWithTag("ExitRigth");
        ball = GameObject.FindGameObjectWithTag("Bola");
        ladoEmX = 0;
        ladoEmY = 1;
        instFormacao = FindObjectOfType<InstaciarFormacao>();
        StartCoroutine("TimeInsBall");
    }

    void Update()
    {

        if (setTime)
            TimeGame();

        SetText();

        if (setBall)
            InstaBall();

        VerificaBall();

        if (ativeTimeFormacao)
            TimeFormacaoPanel();

        camadaGelo = GameObject.FindGameObjectWithTag("cm");
        
    }
    
    void TimeGame()
    {
        time += Time.deltaTime;

        if (time >= 120)
        {
            setTime = false;
            pingooal.SetActive(false);
            VerifiPontos();
        }
        if (time >= 56 && time <= 60f && partidaExecutePanel)
        {
            ball.GetComponent<Ball>().ColocaAlfa(001);
        }
        if (time >= 60 && time <= 60.1f && partidaExecutePanel)
        {
            AtivePanelFormacao();
            ball.GetComponent<Ball>().TiraAlfa();
        }
    }

    public GameObject[] placarInformativo;
    public Text timeText;
    public Text pPlayerText;
    public Text pBotText;
    void SetText()
    {
        timeText.text = time.ToString("000");
        pPlayerText.text = pPlayer.ToString("00");
        pBotText.text = pBot.ToString("00");
        timeFecharText.text = timeFechar.ToString("00");
    }

    public bool gameonline;
    private int forceEmX;
    private int forceEmY = 20;
    void InstaBall()
    {
        ExitBall();
        ball.transform.position = new Vector3(ladoExit, ladoEmY, 0);

        if (!gameonline)
        {
            ball.GetComponent<Rigidbody2D>().velocity = new Vector2(ladoExit * forceEmX, -ball.transform.position.y * forceEmY);
        }
       else
        {
            ball.GetComponent<Rigidbody2D>().velocity = new Vector2(ladoExit, -ball.transform.position.y);
        }
        setBall = false;
    }

    void ExitBall()
    {
        if (ladoEmX == 0)
        {
            ladoExit = exitLeft.transform.position.x;
            forceEmX = 8;
        }
        if (ladoEmX == 1)
        {
            ladoExit = exitRigth.transform.position.x;
            forceEmX = 25;
        }

    }

    void VerifiPontos()
    {
        if (pPlayer > pBot)
        {
            RestartGame("ganhou");
        }
        if (pPlayer < pBot)
        {
            RestartGame("perdeu");
        }
        if (pPlayer == pBot)
        {
            RestartGame("empate");
        }
    }

    IEnumerator TimeInsBall()
    {
        yield return new WaitForSeconds(0.8f);
        setBall = true;
    }
    
    public GameObject[] buttomReplay;
    void RestartGame(string msg)
    {
        if(msg == "ganhou")
            placarInformativo[0].SetActive(true);
        if (msg == "perdeu")
            placarInformativo[1].SetActive(true);
        if (msg == "empate")
            placarInformativo[2].SetActive(true);
       
        foreach(GameObject g in buttomReplay)
        {
            g.SetActive(true);
        }
        ball.transform.position = new Vector3(-7.54f, 0f, 0f);
        this.enabled = false;
    }

    private GameObject[] g;
    void VerificaBall()
    {
        g = GameObject.FindGameObjectsWithTag("Bola");

        if (g.Length >= 2)
        {
            Destroy(g[0]);
        }
    }

    [Header("Formação")]
    public GameObject[] panelFormacao;
    private bool panelAtive;
    private InstaciarFormacao instFormacao;
    private GameObject[] palhetasDestroi;
    private GameObject goleiro;
    private GameObject[] arena;
    private float timeFechar = 15;
    private bool ativeTimeFormacao;
    public Text timeFecharText;
    private EscolhaFormacao escolhaFormacao;
    public GameObject camadaGelo;
    void AtivePanelFormacao()
    {
        ativeTimeFormacao = true;
        setTime = false;
        pingooal.SetActive(false);
        ball.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeAll;
        ball.GetComponent<SpriteRenderer>().sortingOrder = 0;
        if (camadaGelo != null)
            camadaGelo.GetComponent<SpriteRenderer>().sortingOrder = 0;
        panelAtive = true;
        palhetasDestroi = GameObject.FindGameObjectsWithTag("Palheta");
        goleiro = GameObject.FindGameObjectWithTag("GoleiroPlayer");
        arena = GameObject.FindGameObjectsWithTag("Arena");

        foreach (GameObject g in panelFormacao)
        {
            g.SetActive(true);
        }
        
        escolhaFormacao = FindObjectOfType<EscolhaFormacao>();
    }

    public void DesativeFormacao()
    {
        escolhaFormacao.Selecionado();
        time = 60.12f;

        int d = palhetasDestroi.Length;
        for(int i = 0; i <= d - 1; i++)
        {
            Destroy(palhetasDestroi[i]);
        }
        Destroy(arena[0]);
        Destroy(arena[1]);
        Destroy(goleiro);
        instFormacao.LoadPrefabs();
        foreach (GameObject g in panelFormacao)
        {
            g.SetActive(false);
        }
        setTime = true;
        if(palhetaGelo!=null)
            palhetaGelo.DesabilitaPalhetas("Bola");
        ball.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.None;
        ball.GetComponent<SpriteRenderer>().sortingOrder = 5;
    }

    void TimeFormacaoPanel()
    {
        timeFechar -= Time.deltaTime;
       
        if (timeFechar <= 0)
        {
            DesativeFormacao();
            ativeTimeFormacao = false;
            timeFechar = 15;
        }
    }

    public GameObject pingooal;
    public void Pingooal()
    {
        pingooal.SetActive(true);
        Invoke("InvokeSetBall", 2f);
       
    }

    void InvokeSetBall()
    {
        setBall = true;
        pingooal.SetActive(false);
    }
}
  
