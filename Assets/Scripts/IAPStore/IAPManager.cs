﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Purchasing;

// Deriving the Purchaser class from IStoreListener enables it to receive messages from Unity Purchasing.
public class IAPManager : MonoBehaviour, IStoreListener
{
    public static IAPManager Instance { get; set; }

    private static IStoreController m_StoreController;          // The Unity Purchasing system.
    private static IExtensionProvider m_StoreExtensionProvider; // The store-specific Purchasing subsystems.

    //igual do console 
    public static string PRODUCT_6000_GOLD = "gold6000";
    public static string PRODUCT_12000_GOLD = "gold12000";
    public static string PRODUCT_18000_GOLD = "gold18000";
    public static string PRODUCT_36000_GOLD = "gold36000";
    public static string PRODUCT_72000_GOLD = "gold72000";

    void Awake()
    {
        Instance = this;
    }
    void Start()
    {
        // If we haven't set up the Unity Purchasing reference
        if (m_StoreController == null)
        {
            // Begin to configure our connection to Purchasing
            InitializePurchasing();
        }
    }

    public void InitializePurchasing()
    {
        // If we have already connected to Purchasing ...
        if (IsInitialized())
        {
            // ... we are done here.
            return;
        }

        // Create a builder, first passing in a suite of Unity provided stores.
         var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());

         builder.AddProduct(PRODUCT_6000_GOLD, ProductType.Consumable);
         builder.AddProduct(PRODUCT_12000_GOLD, ProductType.Consumable);
         builder.AddProduct(PRODUCT_18000_GOLD, ProductType.Consumable);
         builder.AddProduct(PRODUCT_36000_GOLD, ProductType.Consumable);
         builder.AddProduct(PRODUCT_72000_GOLD, ProductType.Consumable);
         UnityPurchasing.Initialize(this, builder);
    }

    private bool IsInitialized()
    {
        // Only say we are initialized if both the Purchasing references are set.
        return m_StoreController != null && m_StoreExtensionProvider != null;
    }

    public void Buy6000Gold()
    {
        BuyProductID(PRODUCT_6000_GOLD);
    }

    public void Buy12000Gold()
    {
        BuyProductID(PRODUCT_12000_GOLD);
    }
    public void Buy18000Gold()
    {
        BuyProductID(PRODUCT_18000_GOLD);
    }
    public void Buy36000Gold()
    {
        BuyProductID(PRODUCT_36000_GOLD);
    }
    public void Buy72000Gold()
    {
        BuyProductID(PRODUCT_72000_GOLD);
    }


    void BuyProductID(string productId)
    {
        // If Purchasing has been initialized ...
        if (IsInitialized())
        {
            // ... look up the Product reference with the general product identifier and the Purchasing 
            // system's products collection.
            Product product = m_StoreController.products.WithID(productId);

            // If the look up found a product for this device's store and that product is ready to be sold ... 
            if (product != null && product.availableToPurchase)
            {
                Debug.Log(string.Format("Purchasing product asychronously: '{0}'", product.definition.id));
                // ... buy the product. Expect a response either through ProcessPurchase or OnPurchaseFailed 
                // asynchronously.
                m_StoreController.InitiatePurchase(product);
            }
            // Otherwise ...
            else
            {
                // ... report the product look-up failure situation  
                Debug.Log("BuyProductID: FAIL. Not purchasing product, either is not found or is not available for purchase");
            }
        }
        // Otherwise ...
        else
        {
            // ... report the fact Purchasing has not succeeded initializing yet. Consider waiting longer or 
            // retrying initiailization.
            Debug.Log("BuyProductID FAIL. Not initialized.");
        }
    }

    public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
    {
        // Purchasing has succeeded initializing. Collect our Purchasing references.
        Debug.Log("OnInitialized: PASS");

        // Overall Purchasing system, configured with products for this application.
        m_StoreController = controller;
        // Store specific subsystem, for accessing device-specific store features.
        m_StoreExtensionProvider = extensions;
    }
    public void OnInitializeFailed(InitializationFailureReason error)
    {
        // Purchasing set-up has not succeeded. Check error for reason. Consider sharing this reason with the user.
        Debug.Log("OnInitializeFailed InitializationFailureReason:" + error);
    }

    public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args)
    {
        if (String.Equals(args.purchasedProduct.definition.id, PRODUCT_6000_GOLD, StringComparison.Ordinal))
        {
            int moedas = PlayerPrefs.GetInt("ApostaTotal");
            int totalMoedas = 6000 + moedas;
            PlayerPrefs.SetInt("ApostaTotal", totalMoedas);
            Debug.Log("Você comprou 6000 moedas, seu total de moedas é : " + totalMoedas);
        }
        if (String.Equals(args.purchasedProduct.definition.id, PRODUCT_12000_GOLD, StringComparison.Ordinal))
        {
            int moedas = PlayerPrefs.GetInt("ApostaTotal");
            int totalMoedas = 12000 + moedas;
            PlayerPrefs.SetInt("ApostaTotal", totalMoedas);
            Debug.Log("Você comprou 12000 moedas, seu total de moedas é : " + totalMoedas);
        }
        if (String.Equals(args.purchasedProduct.definition.id, PRODUCT_18000_GOLD, StringComparison.Ordinal))
        {
            int moedas = PlayerPrefs.GetInt("ApostaTotal");
            int totalMoedas = 18000 + moedas;
            PlayerPrefs.SetInt("ApostaTotal", totalMoedas);
            Debug.Log("Você comprou 18000 moedas, seu total de moedas é : " + totalMoedas);
        }
        if (String.Equals(args.purchasedProduct.definition.id, PRODUCT_36000_GOLD, StringComparison.Ordinal))
        {
            int moedas = PlayerPrefs.GetInt("ApostaTotal");
            int totalMoedas = 36000 + moedas;
            PlayerPrefs.SetInt("ApostaTotal", totalMoedas);
            Debug.Log("Você comprou 36000 moedas, seu total de moedas é : " + totalMoedas);
        }
        if (String.Equals(args.purchasedProduct.definition.id, PRODUCT_72000_GOLD, StringComparison.Ordinal))
        {
            int moedas = PlayerPrefs.GetInt("ApostaTotal");
            int totalMoedas = 72000 + moedas;
            PlayerPrefs.SetInt("ApostaTotal", totalMoedas);
            Debug.Log("Você comprou 72000 moedas, seu total de moedas é : " + totalMoedas);
        }
        else
        {
            Debug.Log(string.Format("ProcessPurchase: FAIL. Unrecognized product: '{0}'", args.purchasedProduct.definition.id));
        }
        return PurchaseProcessingResult.Complete;
    }

    public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
    {
        // A product purchase attempt did not succeed. Check failureReason for more detail. Consider sharing 
        // this reason with the user to guide their troubleshooting actions.
        Debug.Log(string.Format("OnPurchaseFailed: FAIL. Product: '{0}', PurchaseFailureReason: {1}", product.definition.storeSpecificId, failureReason));
    }
}

